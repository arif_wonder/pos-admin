<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDevice extends Model
{
    protected $fillable = [ 'client_id', 'device_type', 'device_sno', 'used'];
}
