<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfigData extends Model
{
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['id', 'title', 'key', 'value'];

    public $timestamps = false;

    /**
     * Get config data
     *
     * @param void
     * @return Config object
     */



}
