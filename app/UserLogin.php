<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

class UserLogin extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $table = 'users_login';
    protected $fillable = ['username', 'email', 'mobile', 'domain', 'password', 'user_id', 'client_id', 'api_token'];

    protected $appends = ['store_detail'];

    protected $hidden = ['password', 'api_token', 'client'];

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function generateToken($token)
    {
        $this->api_token = $token;
        $this->save();

        return $this->api_token;
    }

    public function getStoreDetailAttribute()
    {
        $data = collect();
        $client = $this->client;
        if ($client) {
            $data->put('max_stores', $client->max_stores);
            $data->put('created_stores', $client->created_stores);
			$data->put('available_limit', $client->max_stores - $client->created_stores);
        }
        return $data;
	}
}
