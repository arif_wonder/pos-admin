<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|string|max:16|unique:clients|unique:users_login|regex:/^[\w]*$/',
            'name' => 'required|string|max:255',
            'max_stores' => 'required|integer|min:1',
            'mobile_cc' => 'required|numeric',
            'message_number_cc' => 'required_with:message_number',
            'mobile' => 'required|digits:10|unique:clients',
            'company_name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:clients|unique:users_login',
            'pan_number' => 'required|string|min:10|max:10|unique:clients',
            'gst_number' => 'required|string|min:15|max:15|unique:clients',
            'active' => 'required|integer|in:0,1',
            'message_number' => 'nullable|digits:10',
            'messaging_key' => 'nullable|uuid',
            'business_type_id' => 'required|exists:business_types,id',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $uname = $this->request->get('username');
            if ((strpos($uname, 'root') !== false) || (strpos($uname, 'admin') !== false) || (strpos($uname, 'master') !== false)) {
                $validator->errors()->add('username', 'Invalid username, root, master and admin words are not allowed');
            }
        });
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'mobile_cc.required' => 'Mobile number\'s country code is required.',
            'message_number_cc.required_with' => 'WhatsApp number\'s country code is required.',
            'business_type_id.required' => 'The business type is field required.',
            'business_type_id.exists' => 'Invali business type is supplied.'
        ];
    }
}
