<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Traits\ClientTrait;
use Illuminate\Support\Facades\Mail;
use App\Mail\ClientCreated;

class ClientController extends Controller
{
    use ClientTrait;

    public function addClient(Request $request)
    {
        // $clientController = app(\App\Http\Controllers\Admin\ClientController::class);
        // return $clientController->addClientInformation($request);
        $user = $request->header('username');
        $password = $request->header('password');

        if (!($user == env('DIST_USER') && $password == env('DIST_PASS'))) {
            return response()->json(['status' => false, 'message' => 'Invalid username/password supplied.']);
        }

        $validator = Validator::make($request->input(),[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:clients,email|unique:users_login,email',
            'max_stores' => 'required|integer|min:1',
            'mobile_cc' => 'required|numeric',
            'wa_number_cc' => 'required_with:wa_number',
            'mobile' => 'required|digits:10|unique:clients,mobile',
            'company_name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'pan_number' => 'required|string|min:10|max:10|unique:clients,pan_number',
            'gst_number' => 'required|string|min:15|max:15|unique:clients,gst_number',
            'active' => 'required|integer|in:0,1',
            'wa_number' => 'nullable|digits:10',
            'messaging_key' => 'nullable|uuid',
            'business_type_id' => 'required|exists:business_types,id',
            'devices' => 'required|size:'.$request->max_stores,
            'devices.*.type' => 'required|in:D180,D200,TPS900,TPS680',
            'devices.*.sno' => 'required'
        ], [
            'mobile_cc.required' => 'Mobile number\'s country code is required',
            'wa_number_cc.required_with' => 'WhatsApp number\'s country code is  required',
            'business_type_id.required' => 'The business type field is required.',
            'business_type_id.exists' => 'Invali business type is supplied.',
            'devices.size' => 'There must be exactly :size :attribute.'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->messages()], 422);
        }
        // return $request->devices;

        $client = new \App\Client();
        $userNameLower = strtolower($request->username);
        $client->name = $request->name;
        $client->username = $userNameLower;
        $client->account_key = $userNameLower;
        $client->email = $request->email;
        $client->max_stores = $request->max_stores;
        $client->mobile_cc = $request->mobile_cc;
        $client->mobile = $request->mobile;
        $client->address = $request->address;
        $client->area = $request->area;
        $client->city = $request->city;
        $client->state = $request->state;
        $client->district = $request->district;
        $client->pincode = $request->pincode;
        $client->company = $request->company_name;
        $client->domain = $request->domain;
        $client->messaging_key = $request->messaging_key;
        $client->message_number_cc = $request->wa_number_cc;
        $client->message_number = $request->wa_number;
        $client->pan_number = $request->pan_number;
        $client->gst_number = $request->gst_number;
        $client->active = $request->active;
        $client->business_type_id = $request->business_type_id;
        // dump($client);

        if ($client->save()) {
            $login = \App\UserLogin::create([
                'user_id' => 1,
                'client_id' => $client->id,
                'username' => $client->username,
                'email' => $client->email,
                'mobile' => $client->mobile,
                'domain' => $client->domain,
                'password' => bcrypt(env('DEFAULT_USER_PASS')),
            ]);
            $devicesArr = [];
            foreach ($request->devices as $device) {
                $devicesArr[] = [
                    'client_id' =>  $client->id,
                    'device_type' => $device['type'],
                    'device_sno'  => $device['sno'],
                    'created_at'  => now(),
                    'updated_at'  => now(),
                ];
            }
            \App\UserDevice::insert($devicesArr);

            if ($this->createNewClientDBfolder($client)) {
                // Send Email
                Mail::to($client->email)->send(new ClientCreated($client));

                return response()->json(['status' => true, 'message' => 'User created successfully.', 'data' => $client], 200);
            }
            return response()->json(['status' => true, 'message' => 'Unable to create user.', 'data' => []], 422);
        } else {
            return response()->json(['status' => true, 'message' => 'Unable to create user.', 'data' => []], 422);
		}
    }

    public function getBusinessTypes()
    {
        $businessTypes = \App\BusinessType::where('active', 1)->get();

        return response()->json(['status' => true, 'data' => $businessTypes], 200);
    }

    public function getClientDevices(Request $request)
    {
        $user = $request->user();
        $devices = $user->client->devices;
        if ($devices->count()) {
            return response()->json(['status' => true, 'data' => $devices->makeHidden('used')], 200);
        }
        return response()->json(['status' => false, 'message' => 'You don\'t have any device.'], 422);
    }
}
