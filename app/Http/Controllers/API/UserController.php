<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\UserCreated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller {
	public function addUser(Request $request) {
		$validator = Validator::make($request->input(), [
			'user_id' => 'required',
			'email' => 'required|email|unique:users_login,email',
			'password' => 'required|confirmed',
			'mobile' => 'required|digits:10|unique:users_login,mobile',
		]);
		if ($validator->fails()) {
			return response()->json(['status' => false, 'message' => $validator->messages()], 422);
		}
		$user = $request->user();
		$client = $user ? $user->client : null;
		if (!$client) {
			return response()->json(['status' => false, 'message' => 'Unauthorized'], 401);
		}
		$userLogin = \App\UserLogin::create([
			'user_id' => $request->user_id,
			'client_id' => $client->id,
			'email' => $request->email,
			'password' => bcrypt($request->password),
			'mobile' => $request->mobile,
			'domain' => $client->domain,
			'username' => $request->username,
		]);
		if ($userLogin) {
			Mail::to($userLogin->email)->send(new UserCreated($userLogin, $request->password));
			return response()->json(['status' => true, 'data' => $userLogin], 200);
		} else {
			return response()->json(['status' => false, 'message' => 'Server Error'], 422);
		}
	}

	public function editUser(Request $request) {
        $user = $request->user();
		$userLogin = \App\UserLogin::where([
            'user_id' => $request->user_id,
            'client_id'  =>  $user->client_id
        ])->first();
		if (!$userLogin) {
			return response()->json(['status' => false, 'message' => 'Invalid user data'], 422);
		}
		$validator = Validator::make($request->input(), [
			'user_id' => 'required',
			'email' => 'required|email|unique:users_login,email,' . $userLogin->id,
			'password' => 'required_with:password_confirmation|confirmed',
			'mobile' => 'required|digits:10|unique:users_login,mobile,' . $userLogin->id,
		]);
		if ($validator->fails()) {
			return response()->json(['status' => false, 'message' => $validator->messages()], 422);
		}
		$user = $request->user();
		$userLogin->email = $request->email ?: $userLogin->email;
		$userLogin->mobile = $request->mobile ?: $userLogin->mobile;
		$userLogin->updated_by = $user->user_id;
		if ($request->password) {
			$userLogin->password = bcrypt($request->password);
		}
		if ($userLogin->save()) {
			return response()->json(['status' => true, 'data' => $userLogin, 'message' => 'User updated successfully.'], 200);
		} else {
			return response()->json(['status' => false, 'message' => 'Server Error'], 422);
		}
    }

    public function deleteUser(Request $request)
    {
        $validator = Validator::make($request->input(), [
			'user_id' => 'required|integer',
		]);
		if ($validator->fails()) {
			return response()->json(['status' => false, 'message' => $validator->messages()], 422);
        }
        $user = $request->user();
		$userLogin = \App\UserLogin::where([
            'user_id' => $request->user_id,
            'client_id'  =>  $user->client_id
        ])->first();
        if ($userLogin) {
            $userLogin->delete();
            return response()->json(['status' => true, 'message' => 'User deleted successfully.'], 200);
        }
        return response()->json(['status' => false, 'message' => 'User not found to delete.'], 422);
    }

	public function loginUser(Request $request) {

		$validator = Validator::make($request->input(), [
			'username' => 'required',
			'password' => 'required',
		]);
		if ($validator->fails()) {
			return response()->json(['status' => false, 'message' => $validator->messages()], 422);
		}

		$field = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
		$user = \App\UserLogin::where([
			$field => $request->username,
		])->first();

		if (!$user) {
			return response()->json(['status' => false, 'message' => 'Invalid Credentials'], 401);
		}
		try {
			if (\Hash::check($request->password, $user->password)) {
				if ($user->client && !$user->client->active) {
					return response()->json(['status' => false, 'message' => 'Your account is deactivated.'], 401);
				}
				$response['token'] = $user->createToken(env('PASSPORT_TOKEN', 'POS_USERS'))->accessToken;
				$response['user'] = $user;
				return response()->json(['status' => true, 'data' => $response]);
			} else {
				return response()->json(['status' => false, 'message' => 'Invalid username or password'], 422);
			}
		} catch (Exception $e) {
			return response()->json(['status' => false, 'message' => 'Failed to create token'], 422);
		}
	}

	/**
	 * Logout function.
	 *
	 * @param Request
	 * @return JSON
	 */
	public function logoutUser(Request $request) {
		try {
			$token = $request->user()->token();
			$token->revoke();
			return response()->json([
				'status' => 'success',
				'message' => "User successfully logged out.",
			]);
		} catch (JWTException $e) {
			return response()->json([
				'status' => 'error',
				'message' => 'Failed to logout, please try again.',
			], 422);
		}
	}

	public function changePassword(Request $request) {
		$validator = Validator::make($request->input(), [
			'password' => 'required|confirmed',
			'user_id' => 'required|integer',
		]);
		if ($validator->fails()) {
			return response()->json(['status' => false, 'message' => $validator->messages()], 422);
		}
		$user = $request->user();
		$client = $user->client;
		$userToChange = \App\UserLogin::where([
			'user_id' => $request->user_id,
			'client_id' => $client->id,
		])->first();
		if (!$userToChange) {
			return response()->json(['status' => true, 'message' => 'User not found for this request.'], 422);
		}
		if ($client->id == $userToChange->client_id) {
			// return response()->json([$user->id, $userToChange->id], 200);
			if ($client && ($user->id == $userToChange->id)) {
				$client->change_pswd = 0;
				$client->save();
			}

			$userToChange->password = bcrypt($request->password);
			$userToChange->updated_by = $user->user_id;
			if ($userToChange->save()) {
				return response()->json(['status' => true, 'message' => 'Password changed successfully.'], 200);
			}
		}
		return response()->json(['status' => false, 'message' => 'Unauthorized'], 422);
	}

	public function updateStoreLimit(Request $request) {
		$validator = Validator::make($request->input(), [
			'action' => 'required|string|in:increase,decrease',
			// 'user_id' => 'required|exists:users_login,user_id'
		], [
			'action.in' => 'Invalid action supplied.',
		]);
		if ($validator->fails()) {
			return response()->json(['status' => false, 'message' => $validator->messages()], 422);
		}
		$user = $request->user();
		$client = $user->client;
		if ($client && $client->max_stores) {
			switch ($request->action) {
			case 'increase':
				if ($client->created_stores < $client->max_stores) {
					$user->client->increment('created_stores');
				} else {
					return response()->json(['status' => false, 'message' => 'Maximum store limit exceeded.'], 422);
				}
				break;
			case 'decrease':
				if ($client->created_stores > 0) {
					$user->client->decrement('created_stores');
				} else {
					goto error;
				}
				break;
			default:
				error:
				return response()->json(['status' => false, 'message' => 'Invalid action supplied.'], 422);
				break;
			}
			return response()->json(['status' => true, 'data' => $user->store_detail], 200);
		}
		return response()->json(['status' => false, 'message' => 'Could not find the associated client detail'], 422);
	}

	public function getCountries() {
		$countries = \File::get(base_path() . '/countries.json');
		$countries = json_decode($countries);
		return response()->json($countries);
	}

	/**
	 * Check if user already exists for the given email id.
	 *
	 * @param Request
	 * @return Response
	 */

	public function checkEmailExists(Request $request) {
		$validator = Validator::make($request->input(), [
			'email' => 'nullable|email',
			'uid' => 'nullable|integer',
			'mobile' => 'nullable|digits:10',
		]);
		if ($validator->fails()) {
			return response()->json(['status' => false, 'message' => $validator->messages()], 422);
		}
		$count = 0;
		// \DB::enableQueryLog();
		if ($request->email && $request->mobile) {
			$countQry = \App\UserLogin::where(function ($query) use ($request) {
				$query->where('email', $request->email)
					->orWhere('mobile', $request->mobile);
			});
		} else {
			if ($request->mobile) {
				$countQry = \App\UserLogin::where('mobile', $request->mobile);
			}
			if ($request->email) {
				$countQry = \App\UserLogin::where('email', $request->email);
			}
		}
		if ($request->uid) {
			$user = $request->user();
			$currentUser = \App\UserLogin::where([
				'user_id' => $request->uid,
				'client_id' => $user->client_id,
			])->first();
			if ($currentUser) {
				$countQry->where('id', '<>', $currentUser->id);
			}
		}
		$count = $countQry->count();
		// dd(\DB::getQueryLog());
		if ($count) {
			return response()->json(['status' => true, 'count' => $count], 200);
		}
		return response()->json(['status' => false, 'count' => $count], 200);
	}
}
