<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Hash;
use Illuminate\Http\Request;

class SettingController extends Controller {
	public function index() {
		$breadcrumbs = [
			['url' => '', 'label' => 'Settings'],
		];
		return view('admin.change_pass', compact('breadcrumbs'));
	}

	/**
	 * Save changed password.
	 */
	public function changePass(Request $request) {
		$user = \Auth::user();
		$this->validate($request, [
			'old_password' => 'required|string|old_password:' . $user->password,
			'password' => 'required|string|min:6|confirmed',
			'password_confirmation' => 'required',
		]);
		$user->password = Hash::make($request->password);
		if ($user->save()) {
			$request->session()->flash('message', 'Password changed successfully.');
			return redirect(route('admin.password.form'));
		} else {
			$request->session()->flash('message', 'Password could not be changed due to some technical reasons.');
			$request->session()->flash('alert-class', 'danger');
			return back()->withInput();
		}
	}

	/**
	 * Update website setting/details.
	 *
	 */
	public function settings(Request $request) {
		$breadcrumbs = [
			['url' => '', 'label' => 'Settings'],
		];
		$settings = \App\ConfigData::all()->toArray();
		$data = collect();
		array_map(function ($arr) use ($data) {
			$data->put($arr['key'], $arr['value']);
		}, $settings);
		$data = $data->toArray();
		if ($request->isMethod('post')) {
			foreach ($request->setting as $key => $val) {
				\App\ConfigData::updateOrCreate(
					['key' => $key],
					['value' => $val]
				);
			}
			$request->session()->flash('message', 'Application settings updated successfully.');
			return redirect(route('admin.setting'));
		}
		return view('admin.settings', compact('breadcrumbs', 'data'));
	}

}
