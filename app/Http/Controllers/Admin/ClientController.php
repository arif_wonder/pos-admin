<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRequest;
use App\Mail\ClientCreated;
use Config;
use Crypt;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ClientController extends Controller {
	/**
	 * Get all clients data.
	 *
	 * @param Illuminate\Http\Request
	 * @return Illuminate\View\View
	 */
	public function index(Request $request) {
		$breadcrumbs = [
			['link' => '', 'label' => 'Home'],
		];
		$clients = Client::where('id', '<>', 0)
			->orderBy('id', 'DESC')->get();
		return view('admin.client.list', compact('breadcrumbs', 'clients'));
	}
	/**
	 * Add new client
	 * @param Illuminate\Http\Request
	 * @return Illuminate\View\View
	 */
	public function addClient(Request $request) {
		$breadcrumbs = [
			['link' => route('admin.client.home'), 'label' => 'Store Admins'],
			['link' => '', 'label' => 'Add New'],
		];
		$countries = \File::get(base_path() . '/countries.json');
		$countries = json_decode($countries);
		$businessTypes = \App\BusinessType::where('active', 1)->get();

		return view('admin.client.add', compact('breadcrumbs', 'countries', 'businessTypes'));
	}

	public function addClientInformation(ClientRequest $request) {
		$client = new Client();
		$userNameLower = strtolower($request->username);
		$client->name = $request->name;
		$client->username = $userNameLower;
		$client->account_key = $userNameLower;
		$client->email = $request->email;
		$client->max_stores = $request->max_stores;
		$client->mobile_cc = $request->mobile_cc;
		$client->mobile = $request->mobile;
		$client->address = $request->address;
		$client->area = $request->area;
		$client->city = $request->city;
		$client->state = $request->state;
		$client->district = $request->district;
		$client->pincode = $request->pincode;
		$client->company = $request->company_name;
		$client->domain = $request->domain;
		$client->messaging_key = $request->messaging_key;
		$client->message_number_cc = $request->message_number_cc;
		$client->message_number = $request->message_number;
		$client->pan_number = $request->pan_number;
		$client->gst_number = $request->gst_number;
		$client->active = $request->active;
		$client->business_type_id = $request->business_type_id;
		// dump($client);

		if ($client->save()) {
			$login = \App\UserLogin::create([
				'user_id' => 1,
				'client_id' => $client->id,
				'username' => $client->username,
				'email' => $client->email,
				'mobile' => $client->mobile,
				'domain' => $client->domain,
				'password' => bcrypt(env('DEFAULT_USER_PASS')),
			]);

			if ($this->createNewClientDBfolder($client)) {
				// Send Email
				Mail::to($client->email)->send(new ClientCreated($client));

				$request->session()->flash('message', 'User created successfully.');
				return redirect(route('admin.client.home'));
			}
			$request->session()->flash('message', 'Unable to create user.');
			$request->session()->flash('alert-class', 'danger');
			return redirect(route('admin.client.home'));

		} else {
			$request->session()->flash('message', 'Unable to create user.');
			$request->session()->flash('alert-class', 'danger');
			return back()->withInput();
		}
	}

	public function updateClientDBinfo($insertedId, $createDBuser, $pass) {
		$createDBname = "pos_" . $insertedId . "_client";
		$pass = env('DB_MASTER_PASS');
		$db_info = array(
			'host' => Crypt::encryptString(env('DB_HOST')),
			'port' => Crypt::encryptString(env('DB_PORT')),
			'db' => Crypt::encryptString($createDBname),
			'user' => Crypt::encryptString($createDBuser),
			'pass' => Crypt::encryptString($pass),
		);

		$db_info = base64_encode(serialize($db_info));

		$client = Client::find($insertedId);
		$client->db_info = $db_info;

		$client->save();
		return $insertedId;
	}

	public function createNewClientDBfolder($user) {
		// start of update file
		$SQLFile = File::get(base_path() . '/my_sql_file/raw-db.sql');
		$DB_HOST = env('DB_HOST');
		$DB_PORT = env('DB_PORT');
		$DB_USERNAME = env('DB_USERNAME');
		$DB_PASSWORD = env('DB_PASSWORD');
		$DB_DATABASE = env('DB_DATABASE');

		$userName = $user->username;
		$insertedId = $user->id;
		$createdUserName = strtolower($userName); // user name

		$createDBname = "pos_" . $insertedId . "_client"; // database name based on inserted id

		$createDBuser = env('DB_MASTER_USER'); // $createdUserName; // trim("root");
		$createDBpass = env('DB_MASTER_PASS');
		$this->updateClientDBinfo($insertedId, $createDBuser, $createDBpass);

		/*DB::statement("CREATE DATABASE " . $createDBname) &&
			        DB::statement("CREATE USER '" . $createDBuser . "'@'localhost' IDENTIFIED BY '" . $createDBpass . "'") &&
		*/

		// DB::statement("FLUSH PRIVILEGES");
		if (
			DB::statement("CREATE DATABASE " . $createDBname) &&
			DB::statement("GRANT ALL PRIVILEGES ON " . $createDBname . ".* TO '" . $createDBuser . "'@'%' WITH GRANT OPTION") &&
			DB::statement("FLUSH PRIVILEGES")
		) {
			$connection = array();
			$connection['host'] = $DB_HOST;
			$connection['username'] = $createDBuser;
			$connection['password'] = $createDBpass;
			$connection['dbname'] = $createDBname;
			$connection['port'] = $DB_PORT;
			$this->setConfigWithConnection($createDBname, $connection);
			DB::unprepared($SQLFile);
			$passwd = md5(env('DEFAULT_USER_PASS'));
			$pref = 'a:4:{s:8:"language";s:7:"english";s:10:"base_color";s:5:"green";s:14:"pos_side_panel";s:5:"right";s:11:"pos_pattern";s:13:"brickwall.jpg";}';
			$insertAdminUserSql = "INSERT INTO `users` (`id`, `group_id`, `username`, `email`, `mobile`, `password`, `preference`) VALUES (1, 1, '" . $userName . "', '" . $user->email . "', '" . $user->mobile . "', '" . $passwd . "', '" . $pref . "')";
			DB::unprepared($insertAdminUserSql);

			// ********** end of the database **************** //

			$reConnection = array();
			$reConnection['host'] = $DB_HOST;
			$reConnection['username'] = $DB_USERNAME;
			$reConnection['password'] = $DB_PASSWORD;
			$reConnection['dbname'] = $DB_DATABASE;
			$reConnection['port'] = $DB_PORT;
			$this->setConfigWithConnection($DB_DATABASE, $reConnection);
			DB::statement("FLUSH PRIVILEGES");
			return DB::connection()->getDatabaseName();
		}
		return false;
	}

	public function editClient($id) {
		$client = Client::find($id);
		$breadcrumbs = [
			['link' => route('admin.client.home'), 'label' => 'Store Admins'],
			['link' => '', 'label' => 'Edit Store Admin'],
		];
		$countries = \File::get(base_path() . '/countries.json');
		$countries = json_decode($countries);
		$businessTypes = \App\BusinessType::where('active', 1)->get();
		return view('admin.client.edit', compact('breadcrumbs', 'client', 'countries', 'businessTypes'));
	}

	public function updateClientInformation(Request $request, $id) {
		$this->validate($request, [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:clients,email,' . $id,
			'max_stores' => 'required|integer|min:1',
			'mobile_cc' => 'required|numeric',
			'message_number_cc' => 'required_with:message_number',
			'mobile' => 'required|digits:10|unique:clients,mobile,' . $id,
			'company_name' => 'required|string|max:255',
			'address' => 'required|string|max:255',
			'pan_number' => 'required|string|min:10|max:10|unique:clients,pan_number,' . $id,
			'gst_number' => 'required|string|min:15|max:15|unique:clients,gst_number,' . $id,
			'active' => 'required|integer|in:0,1',
			'message_number' => 'nullable|digits:10',
			'messaging_key' => 'nullable|uuid',
			'business_type_id' => 'required|exists:business_types,id',
		], [
			'mobile_cc.required' => 'Mobile number\'s country code is required',
			'message_number_cc.required_with' => 'WhatsApp number\'s country code is required',
			'business_type_id.required' => 'The business type field is required.',
			'business_type_id.exists' => 'Invali business type is supplied.',
		]);

		$client = Client::find($id);

		$client->name = $request->name;
		$client->email = $request->email;
		$client->max_stores = $request->max_stores;
		$client->mobile_cc = $request->mobile_cc;
		$client->mobile = $request->mobile;
		$client->address = $request->address;
		$client->area = $request->area;
		$client->city = $request->city;
		$client->state = $request->state;
		$client->district = $request->district;
		$client->pincode = $request->pincode;
		$client->company = $request->company_name;
		$client->domain = $request->domain;
		$client->messaging_key = $request->messaging_key;
		$client->message_number_cc = $request->message_number_cc;
		$client->message_number = $request->message_number;
		$client->pan_number = $request->pan_number;
		$client->gst_number = $request->gst_number;
		$client->active = $request->active;
		$client->business_type_id = $request->business_type_id;

		if ($client->save()) {
			$request->session()->flash('message', 'Client records updated successfully.');
			return redirect(route('admin.client.home'));
		} else {
			$request->session()->flash('message', 'Unable to update Client records.');
			$request->session()->flash('alert-class', 'danger');
			return back()->withInput();
		}
	}

	public function setConfigWithConnection($dbname, $connection) {
		Config::set('database.connections.' . $dbname . '.driver', 'mysql');
		Config::set('database.connections.' . $dbname . '.charset', 'utf8mb4');
		Config::set('database.connections.' . $dbname . '.collation', 'utf8mb4_unicode_ci');
		Config::set('database.connections.' . $dbname . '.timezone', '+00:00');
		Config::set('database.connections.' . $dbname . '.host', $connection['host']);
		Config::set('database.connections.' . $dbname . '.username', $connection['username']);
		Config::set('database.connections.' . $dbname . '.password', $connection['password']);
		Config::set('database.connections.' . $dbname . '.database', $connection['dbname']);
		Config::set('database.connections.' . $dbname . '.port', $connection['port']);
		Config::set('database.connections.' . $dbname . '.strict', false);
		Config::set('database.default', $dbname);
		return DB::connection()->getDatabaseName();
	}

	/**
	 * Update user status.
	 *
	 * @param int id
	 * @param int status
	 * @return void
	 */
	public function updateStatus(Request $request, $id, $status = '1') {
		$statusArr = ['deactivated', 'activated'];
		$client = Client::find($id);
		$client->active = $status;
		if ($client->save()) {
			$request->session()->flash('message', 'Client ' . $statusArr[$status] . ' successfully.');
			return redirect(route('admin.client.home'));
		} else {
			$request->session()->flash('message', 'Unable to update client status.');
			$request->session()->flash('alert-class', 'danger');
			return redirect(route('admin.client.home'));
		}
	}

}
