<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BusinessTypeController extends Controller
{
    /**
	 * Get all business types.
	 *
	 * @param Illuminate\Http\Request
	 * @return Illuminate\View\View
	 */
    public function index(Request $request)
    {
        $breadcrumbs = [
            ['link' => '', 'label' => 'Home'],
        ];
        $types = \App\BusinessType::orderBy('id', 'DESC')->get();
        return view('admin.types.list', compact('breadcrumbs', 'types'));
    }

    /**
	 * Add new business types
	 * @param Illuminate\Http\Request
	 * @return Illuminate\View\View
	 */
    public function addType(Request $request)
    {
        if ($request->isMethod('post')) {
            $this->validate($request, [
                'name'  =>  'required|max:255',
                'active'  =>  'required|in:0,1',
            ]);
            $type = \App\BusinessType::create([
                'name'  =>  $request->name,
                'slug'  =>  $this->getSlug($request->name, \App\BusinessType::class),
                'active' => $request->active
            ]);
            if ($type) {
                $request->session()->flash('message', 'Business types created successfully.');
                return redirect(route('admin.business.home'));
            }
        }
        $breadcrumbs = [
            ['link' => route('admin.business.home'), 'label' => 'Business Types'],
            ['link' => '', 'label' => 'Add New'],
        ];
        return view('admin.types.add', compact('breadcrumbs'));
    }

    /**
	 * Update business type status.
	 *
	 * @param int id
	 * @param int status
	 * @return void
	 */
    public function updateType(Request $request, $id)
    {
        if ($type = \App\BusinessType::find($id)) {
            if ($request->isMethod('post')) {
                $this->validate($request, [
                    'name'  =>  'required|max:255',
                    'active'  =>  'required|in:0,1',
                ]);
                $type->name = $request->name;
                $type->active = $request->active;
                if ($type->save()) {
                    $request->session()->flash('message', 'Business types updated successfully.');
                    return redirect(route('admin.business.home'));
                }
            }
            $breadcrumbs = [
                ['link' => route('admin.business.home'), 'label' => 'Business Types'],
                ['link' => '', 'label' => 'Edit'],
            ];
            return view('admin.types.edit', compact('breadcrumbs', 'type'));
        }
        $request->session()->flash('message', 'Business type not found.');
        $request->session()->flash('alert-class', 'danger');
        return redirect(route('admin.business.home'));
    }


    /**
	 * Update business type status.
	 *
	 * @param int id
	 * @param int status
	 * @return void
	 */
    public function updateStatus(Request $request, $id, $status = '1')
    {
        $statusArr = ['deactivated', 'activated'];
        $client = \App\BusinessType::find($id);
        $client->active = $status;
        if ($client->save()) {
            $request->session()->flash('message', 'Business type ' . $statusArr[$status] . ' successfully.');
            return redirect(route('admin.business.home'));
        } else {
            $request->session()->flash('message', 'Unable to update business type status.');
            $request->session()->flash('alert-class', 'danger');
            return redirect(route('admin.business.home'));
		}
    }

    /**
	 * Delete business type status.
	 *
	 * @param int id
	 * @param int status
	 * @return void
	 */
    public function deleteType(Request $request)
    {
        if ($type = \App\BusinessType::find($request->id)) {
            if ($type->delete()) {
                $request->session()->flash('message', 'Business type deleted successfully.');
                return redirect(route('admin.business.home'));
            }
        }
        $request->session()->flash('message', 'Unable to delete business type.');
        $request->session()->flash('alert-class', 'danger');
        return redirect(route('admin.business.home'));
    }
}
