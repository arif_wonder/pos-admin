<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;

class HomeController extends Controller
{
    public function index()
    {
    	$breadcrumbs = [
    		['link' => '', 'label' => 'Home']
    	];
    	$clients = Client::where('id','<>',0)
                        ->orderBy('id', 'DESC')->count();
    	return view('admin.home', compact('breadcrumbs','clients'));
    }
}
