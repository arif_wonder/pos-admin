<?php
namespace App\Http\Controllers;

use App\BankDetail;
use App\candidateUser;
use App\Http\Controllers\Controller;
use App\Mail\ResetOtpMail;
use App\ResetOtp;
use App\User;
use Auth;
use DB;
use Edujugon\PushNotification\PushNotification;
use File;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use JWTAuthException;
use Mail;

class UserController extends Controller
{
    private $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'gender' => 'in:male,female',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->messages()], 200);
        }
        $token = null;
        $user = $this->user->create([
            'name' => $request->get('name') ?: null,
            'latitude' => $request->get('latitude') ?: null,
            'longitude' => $request->get('longitude') ?: null,
            'gender' => $request->get('gender') ?: 'male',
            'mobile' => $request->get('mobile') ?: null,
            'address' => $request->get('address') ?: null,
            'firebase_id' => $request->get('firebase_id') ?: null,
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'role' => $request->get('role') ?: 'candidate',
            'company_name' => $request->get('company_name') ?: null,
            'website' => $request->get('website') ?: null,
        ]);
        if ($token = JWTAuth::fromUser($user)) {
            if (count($request->categories)) {
                $user->categories()->attach($request->categories);
            }
            $user->generateToken($token);
            return response()->json(['status' => true, 'message' => 'User created successfully', 'data' => $user], 200);
        }
        return response()->json(['status' => false, 'message' => 'Couldn\'t login'], 200);
    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->input(), [
            'username' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->messages()], 200);
        }

        $field = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $credentials = [$field => $request->username, 'password' => $request->password];
        $token = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['status' => false, 'message' => 'Invalid email or password'], 200);
            } else {
                Auth::user()->generateToken($token);
                return response()->json(['status' => true, 'data' => Auth::user()]);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Failed to create token'], 200);
        }
    }
    public function checkEmailLogin(Request $request)
    {
        $user = User::where('email', $request->input('email'))->first();
        $token = null;
        if ($user) {
            try {
                if (!$token = JWTAuth::fromUser($user)) {
                    return response()->json(['status' => false, 'message' => 'Invalid email address'], 200);
                }
            } catch (JWTAuthException $e) {
                return response()->json(['status' => false, 'message' => 'Failed to create token'], 200);
            }
            $user->generateToken($token);
            return response()->json(['status' => true, 'data' => $user], 200);
        } else {
            return response()->json(['status' => false, 'message' => 'Invalid email address'], 200);
        }
    }
    public function getAuthUser(Request $request)
    {

        try {
            $user = JWTAuth::toUser($request->token);
            switch ($user->role) {
                case 'candidate':
                    $appliedJobs = $user->appliedJobs()->pluck('job_id')->toArray();
                    $job_within = $user->job_within ?: 10;
                    $open = \App\Job::select(
                        'jobs.*', \DB::raw('69.0 * DEGREES(ACOS(COS(RADIANS(' . $user->latitude . '))
                    * COS(RADIANS(jobs.latitude))
                    * COS(RADIANS(' . $user->longitude . ' - jobs.longitude))
                    + SIN(RADIANS(' . $user->latitude . '))
                    * SIN(RADIANS(jobs.latitude)))) as distance')
                    )
                        ->leftJoin('category_user as cu', 'cu.category_id', '=', 'jobs.category_id')
                        ->leftJoin('applied_jobs as aj', 'aj.job_id', '=', 'jobs.id')
                        ->where([
                            ['cu.user_id', $user->id],
                            ['jobs.status', 'open'],
                        ])
                        ->havingRaw('distance <= ' . $job_within)
                        ->whereNotIn('jobs.id', $appliedJobs)
                        ->groupBy('jobs.id')
                        ->get();
                    $user->open_count = count($open);

                    $hiredJobs = $user->candidateHiredJobs()->pluck('job_id')->toArray();
                    $applied = \App\Job::select('jobs.*', 'aj.user_rate')
                        ->leftJoin('applied_jobs as aj', 'aj.job_id', '=', 'jobs.id')
                        ->groupBy('jobs.id')
                        ->where('aj.user_id', $user->id)
                        ->whereNotIn('aj.job_id', $hiredJobs)
                        ->where('jobs.status', '<>', 'completed')
                        ->get();
                    $user->applied_count = count($applied);

                    $awarded = \App\Job::select('jobs.*', 'aj.user_rate', 'hj.start_time as work_start_time', 'hj.status as work_status', 'hj.completed_time as work_completed_time')
                        ->leftJoin('applied_jobs as aj', 'aj.job_id', '=', 'jobs.id')
                        ->join('hired_jobs as hj', 'hj.job_id', '=', 'jobs.id')
                        ->groupBy('jobs.id')
                        ->where('hj.candidate_id', $user->id)
                        ->where('jobs.status', 'running')
                        ->get();
                    $user->awarded_count = count($awarded);

                    $completed = \App\Job::select('jobs.*', 'hj.start_time as work_start_time', 'hj.status as work_status', 'hj.completed_time as work_completed_time')
                    //->leftJoin('applied_jobs as aj', 'aj.job_id', '=', 'jobs.id')
                        ->leftJoin('hired_jobs as hj', 'hj.job_id', '=', 'jobs.id')
                        ->groupBy('jobs.id')
                        ->where([
                            ['hj.candidate_id', $user->id],
                            ['jobs.status', 'completed'],
                            ['hj.status', 'completed'],
                        ])
                        ->whereNotNull('hj.completed_time')
                        ->where('jobs.status', 'completed')
                        ->get();
                    $user->completed_count = count($completed);

                    $cancel = \App\Job::select('jobs.*')
                        ->leftJoin('users as u', 'u.id', '=', 'jobs.cancelled_by')
                        ->leftJoin('applied_jobs as aj', 'aj.job_id', '=', 'jobs.id')
                        ->where('aj.user_id', $user->id)
                        ->where('jobs.status', 'cancelled')->orwhere('jobs.status', 'refunded')
                        ->orderBy('jobs.updated_at', 'desc')
                        ->groupBy('jobs.id')
                        ->groupBy('jobs.id')
                        ->get();
                    $user->cancel_count = count($cancel);
                    break;

                case 'employer':
                    $open = $user->jobs()->where([
                        ['active', '1'],
                        ['status', 'open'],
                    ])
                        ->get();
                    $user->open_count = count($open);
                    /************** RUNNING JOBS ******** */
                    $running = $user->jobs()->select('jobs.*', 'hj.candidate_id as cand_id')
                        ->join('hired_jobs as hj', 'hj.job_id', '=', 'jobs.id')
                        ->where([
                            ['jobs.active', '1'],
                            ['jobs.status', 'running'],
                        ])
                        ->get();
                    $user->running_count = count($running);
                    /************* COMPLETED JOBS ******** */
                    $completed = $user->jobs()->select('jobs.*', 'hj.candidate_id', 'hj.start_time as work_start_time', 'hj.completed_time as work_completed_time')
                        ->join('hired_jobs as hj', 'hj.job_id', '=', 'jobs.id')
                        ->where([
                            ['jobs.active', '1'],
                            ['jobs.status', 'completed'],
                            ['hj.status', 'completed'],
                        ])
                        ->whereNotNull('hj.completed_time')
                        ->get();
                    $user->completed_count = count($completed);
                    /************* EXPIRED JOBS ******** */
                    $expired = $user->jobs()->where([
                        ['active', '1'],
                        ['status', 'expired'],
                    ])
                        ->get();
                    $user->expired_count = count($expired);

                    $cancel = $user->jobs()
                        ->where('status', 'cancelled')
                        ->orwhere('status', 'refunded')
                        ->get();
                    $user->cancel_count = count($cancel);
                    break;
            }
            return response()->json(['status' => true, 'data' => $user], 200);
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }
    /**
     * Logout function.
     *
     * @param Request
     * @return JSON
     */
    public function logoutUser(Request $request)
    {
        if (JWTAuth::invalidate($request->token)) {
            return response()->json(['status' => true, 'message' => 'Logged out successfully.'], 200);
        }
        return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
    }
    /**
     * Get user reviews.
     *
     * @param Request
     * @return JSON
     */
    public function userRating(Request $request)
    {
        try {
            if ($request->userid) {
                $user = User::find($request->userid);
            } else {
                $user = JWTAuth::toUser($request->token);
            }
            $resp['rating'] = $user->reviews->avg('rating');
            $resp['count'] = $user->reviews->count();
            $resp['status'] = true;
            return response()->json($resp, 200);
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }
    /**
     * Update user profile picture.
     *
     * @param Request
     * @return JSON
     */
    public function updateProfile(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $base64Img = explode(',', $request->profile_img);
            //@list(, $file_data) = explode(',', $request->profile_img);
            $ext = 'jpg';
            $image = base64_decode($request->profile_img);
            $imgName = time() . '.' . $ext;
            $destFolder = public_path('uploads/profile');
            if (!File::exists($destFolder)) {
                File::makeDirectory($destFolder, 0777, true, true);
            }
            if (File::isFile(str_replace(url('uploads/profile'), $destFolder, $user->profile_img))) {
                @unlink(str_replace(url('uploads/profile'), $destFolder, $user->profile_img));
            }
            @file_put_contents($destFolder . '/' . $imgName, $image);
            $user->profile_img = $imgName;
            if ($user->save()) {
                return response()->json(['status' => true, 'message' => 'Profile updated successfully.', 'url' => $user->profile_img], 200);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }
    /**
     * Update user intro video.
     *
     * @param Request
     * @return JSON
     */
    public function updateVideo(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $destFolder = public_path('uploads/intro_vids');
            $video = $request->file('video');
            if (!File::exists($destFolder)) {
                File::makeDirectory($destFolder, 0777, true, true);
            }
            if (File::isFile(str_replace(url('uploads/intro_vids'), $destFolder, $user->intro_video))) {
                @unlink(str_replace(url('uploads/intro_vids'), $destFolder, $user->intro_video));
            }
            $vidName = time() . '.' . $video->getClientOriginalExtension();
            $video->move($destFolder, $vidName);
            $user->intro_video = $vidName;
            if ($user->save()) {
                return response()->json(['status' => true, 'message' => 'Profile updated successfully.', 'url' => $user->intro_video], 200);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }
    /**
     * Update user details.
     *
     * @param Request
     * @return JSON
     */
    public function updateUser(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $validator = Validator::make($request->input(), [
                'email' => 'string|email|max:255|unique:users,email,' . $user->id,
                'gender' => 'in:male,female',
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->messages()], 200);
            }
            $user->name = $request->name ?: $user->name;
            $user->email = $request->email ?: $user->email;
            $user->mobile = $request->mobile ?: $user->mobile;
            $user->gender = $request->gender ?: $user->gender;
            $user->address = $request->address ?: $user->address;
            $user->latitude = $request->latitude ?: $user->latitude;
            $user->longitude = $request->longitude ?: $user->longitude;
            $user->job_within = $request->job_within ?: $user->job_within;
            $user->company_name = $request->company_name ?: $user->company_name;
            $user->website = $request->website ?: $user->website;
            $user->about = $request->about ?: $user->about;
            $user->firebase_id = $request->firebase_id ?: $user->firebase_id;
            if ($user->save()) {
                if (count($request->categories)) {
                    $cats = $request->categories ?: [];
                    $user->categories()->sync($cats);
                }
                return response()->json(['status' => true, 'data' => $user], 200);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }
    /**
     * Request for password reset otp.
     *
     * @param Request
     * @return JSON
     */
    public function passwordRequest(Request $request)
    {
        try {
            $user = User::where('email', $request->email)->first();
            if (!$user) {
                return response()->json(['status' => false, 'message' => 'Email id doesn\'t exists.']);
            }
            DB::table('reset_otp')
                ->where('email', $user->email)
                ->update(['expired' => '1']);
            $reset = new ResetOtp;
            $reset->email = $user->email;
            $reset->otp = mt_rand(111111, 999999);
            $reset->save();
            // Send OTP to user.
            Mail::to($user)->send(new ResetOtpMail($reset, $user->name));
            return response()->json(['status' => true, 'message' => 'OTP has been sent to the registered email id.'], 200);
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }
    /**
     * update password using OTP that was sent to email.
     *
     * @param Request
     * @return JSON
     */
    public function passwordUpdate(Request $request)
    {
        try {
            // $user = JWTAuth::toUser($request->token);
            $validator = Validator::make($request->input(), [
                'otp' => 'required',
                'password' => 'required|string|min:6',
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->messages()], 200);
            }
            $otpData = DB::table('reset_otp')
                ->where([
                    ['otp', $request->otp],
                    ['expired', '0'],
                ])->latest()->first();
            if (!$otpData) {
                return response()->json(['status' => false, 'message' => 'Invalid OTP supplied.']);
            }
            $user = User::where('email', $otpData->email)->first();
            $now = \Carbon\Carbon::now();
            $time = \Carbon\Carbon::parse($otpData->created_at);
            $diff = $time->diffInHours($now);
            if ($diff > 28) {
                DB::table('reset_otp')
                    ->where('email', $user->email)
                    ->update(['expired' => '1']);
                return response()->json(['status' => false, 'message' => 'OTP has been expired.']);
            }

            $user->password = bcrypt($request->password);
            $user->save();
            DB::table('reset_otp')
                ->where('email', $user->email)
                ->update(['expired' => '1']);
            return response()->json(['status' => true, 'message' => 'Password updated successfully.'], 200);
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }

    /**
     * Get bank details along with secret key.
     *
     * @param Request
     * @return JSON
     */
    public function getBankDetails(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $bank = $user ? $user->bankDetail : null;
            if ($bank) {
                $key = explode(':', env('APP_KEY'));
                //$bank->key = $key[1];
            } else {
                $bank = new BankDetail;
            }
            return response()->json(['status' => true, 'data' => $bank], 200);
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }
    /**
     * Upload candidate resume.
     *
     * @param Request
     * @return JSON
     */
    public function uploadFile(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'type' => 'required|in:resume,medical_report,drugtest_report',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->messages()], 200);
        }
        try {
            $user = JWTAuth::toUser($request->token);
            $type = $request->type;
            $folder = ['resume' => 'resumes', 'medical_report' => 'reports', 'drugtest_report' => 'reports'];
            $destFolder = public_path('uploads/' . $folder[$type]);
            $resume = $request->file('file');
            if (!File::exists($destFolder)) {
                File::makeDirectory($destFolder, 0777, true, true);
            }
            if ($user->candidateData && File::isFile(str_replace(url('/uploads/' . $folder[$type]), $destFolder, $user->candidateData->$type))) {
                @unlink(str_replace(url('/uploads/' . $folder[$type]), $destFolder, $user->candidateData->$type));
            }
            $ext = $resume->getClientOriginalExtension();
            $resumeName = time();
            $resume->move($destFolder, $resumeName . '.' . $ext);
            if ($ext == 'pdf') {
                $pdf = new \Spatie\PdfToImage\Pdf($destFolder . '/' . $resumeName . '.' . $ext);
                $pdf->saveImage($destFolder . '/' . $resumeName . '_thumb.jpg');
            }
            $resumeName = $resumeName . '.' . $ext;
            if ($user->candidateData) {
                $candidateUser = candidateUser::find($user->candidateData->id);
                $candidateUser->$type = $resumeName;
            } else {
                $candidateUser = new candidateUser([
                    $type => $resumeName,
                ]);
            }
            $user->candidateData()->save($candidateUser);
            return response()->json(['status' => true, 'data' => $user], 200);
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }
    /**
     * Change user password.
     *
     * @param Request
     * @return JSON
     */
    public function changePassword(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            //print_r($user);
            $validator = Validator::make($request->input(), [
                'old_password' => 'required|string|old_password:' . $user->password,
                'password' => 'required|string|min:6',
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->messages()], 200);
            }
            $user->password = Hash::make($request->password);
            if ($user->save()) {
                return response()->json(['status' => true, 'message' => 'Password changed successfully.'], 200);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }
    /**
     * Get user rating.
     *
     * @param Request
     * @return JSON
     */
    public function getUserRating(Request $request)
    {
        if (!$request->userid) {
            return response()->json(['status' => false, 'message' => 'User ID is required.'], 200);
        }
        try {
            $user = User::find($request->userid);
            $resp = \App\UserReview::where('user_id', $user->id)
                ->paginate(20);
            return response()->json(['status' => true, 'data' => $resp], 200);
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }
    /**
     * Save user's card.
     *
     * @param Request
     * @return JSON
     */
    public function saveCard(Request $request)
    {
        try {

            $user = JWTAuth::toUser($request->token);
            $resp = \App\Card::create([
                'user_id' => $user->id,
                'card_no' => $request->card_no,
                'exp_month' => $request->exp_month,
                'exp_year' => $request->exp_year,
            ]);
            return response()->json(['status' => true, 'data' => $resp], 200);
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }

    /**
     * Save user's card.
     *
     * @param Request
     * @return JSON
     */
    public function getCards(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $cards = $user->cards()->get();
            return response()->json(['status' => true, 'data' => $cards], 200);
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }
    public function deleteCard(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $validator = Validator::make($request->input(), ['card_id' => 'required']);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->messages()], 200);
            }
            $user->cards()->where("id", $request->card_id)->delete();
            return response()->json(['status' => true, 'message' => "Card has been deleted"], 200);
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }
    public function getServiceCharge(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $service_charge = \App\ConfigData::where("key", "emp_payin_charge")->orwhere("key", "emp_payout_charge")->orwhere("key", "cand_payout_charge")->get();
            return response()->json(['status' => true, 'service_charge' => $service_charge], 200);
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }
    public function updateIntroVideo(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $validator = Validator::make($request->input(), ['intro_video' => 'required']);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->messages()], 200);
            }
            $user->intro_video = $request->intro_video;
            if ($user->save()) {
                return response()->json(['status' => true, 'message' => 'Video has been saved.'], 200);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }
    public function postEnquiry(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $validator = Validator::make($request->input(), ['comment' => 'required']);
            if ($validator->fails()) {
                return response()->json(['status' => false, 'message' => $validator->messages()], 200);
            }
            $response = \App\Contact::create(array("user_id" => $user->id, "name" => $user->name, "mobile" => $user->mobile, "email" => $user->email, "comment" => $request->comment, "status" => "Open"));
            return response()->json(['status' => true, 'message' => "Your enquiry placed successfully."], 200);
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }
    public function getEnquiry(Request $request)
    {
        try {
            $user = JWTAuth::toUser($request->token);
            $result = \App\Contact::where(array("user_id" => $user->id))->orderBy("created_at", "DESC");
            return response()->json(['status' => true, 'result' => $result], 200);
        } catch (JWTAuthException $e) {
            return response()->json(['status' => false, 'message' => 'Invalid token'], 200);
        }
    }

    public function webHook()
    {
        $input = @file_get_contents("php://input");
        $respone = json_decode($input);
        if ($respone->type == 'payout.paid') {
            $payout_id = $respone->data->object->id;
            $status = $respone->data->object->status;
            if ($payout_id != '' && $status == 'paid') {
                \App\Payout::where("response_id", $payout_id)->update(["status" => $status]);
                $payout_info = \App\Payout::where('response_id', $payout_id)->first();
                if (!$payout_info) {
                    return;
                }
                $user_id = $payout_info->user_id;
                $user_info = \App\User::where(array("id" => $user_id))->first();
                $role = $user_info->role;
                $amount = $payout_info->amount;
                $description = "Payout to card/bank account";
                $wallet_amount = $amount;
                $action = 2;
                $transaction_info = \App\WalletTransaction::create(array(
                    'user_id' => $user_id, 'amount' => $wallet_amount, 'action' => $action, 'summary' => $description, 'created_at' => date("Y-m-d H:i:s")));
                $wallet_info = \App\Wallet::where('user_id', $user_id)->first();
                $users_id = $wallet_info['user_id'];
                $balance = $wallet_info['balance'];
                $balance -= $wallet_amount;
                \App\Wallet::where('user_id', $user_id)->update(array('balance' => $balance, 'updated_at' => date("Y-m-d H:i:s")));
                $notification = \App\Notification::create(
                    [
                        'user_id' => $user_id,
                        'title' => config('app.name', 'Helpknx'),
                        'description' => __('notif.wallet_debited', ['amount' => $wallet_amount]),
                        'meta_data' => json_encode(['job_id' => "", 'tab' => 'wallet_' . $role]),
                    ]
                );
                $push = new PushNotification('fcm');
                $push->setMessage([
                    //'message'   =>  [
                    'notification' => [
                        'title' => config('app.name', 'Helpknx'),
                        'body' => __('notif.wallet_debited', ['amount' => $wallet_amount]),
                        'sound' => 'default',
                    ],
                    'data' => [
                        'job_id' => '',
                        'tab' => 'wallet_' . $role,
                        'notif_id' => $notification->id,
                    ],
                    //]
                ])
                    ->setDevicesToken($user->firebase_id)
                    ->send();
            }
        }
    }

}
