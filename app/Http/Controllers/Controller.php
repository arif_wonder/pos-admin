<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Get Unique Slug From Title
     */
     protected function getSlug($title, $model)
     {
         if (! is_subclass_of($model, 'Illuminate\Database\Eloquent\Model')) {
             throw new \Exception("Invalid model instance supplied.");
         }
         $slug = str_slug($title, '-');
         $count = $model::whereRaw("slug REGEXP '^{$slug}(-[0-9]*)?$'")->count();
         return ($count > 0) ? $slug.'-'.$count : $slug;
     }

    public function generatePassword($length)
    {
        $alphabet = '!@#$^&*()-+?abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$^&*()-+?';
        $pass = [];
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = mt_rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }
}
