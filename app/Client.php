<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;
use Crypt;

class Client extends Model {
    protected $guarded = [];

	/**
	 * Append attribute.
	 */
	// protected $appends = ['db_info'];

	public function setAccountKeyAttribute($value) {
		try {
            $this->attributes['account_key'] = Uuid::uuid3(Uuid::NAMESPACE_DNS, $value);
		}
		catch (UnsatisfiedDependencyException $e) {
			echo 'Caught exception: ' . $e->getMessage() . "\n";
		}
    }

    public function users()
    {
        return $this->hasMany('App\UserLogin');
    }

    public function devices()
    {
        return $this->hasMany('App\UserDevice');
    }

    public function businessType()
    {
        return $this->belongsTo('App\BusinessType');
    }

    public function getDbInfoAttribute()
    {
        $dbInfo = $this->attributes['db_info'];
        $dbInfo = unserialize(base64_decode($dbInfo));
        // dump($dbInfo);
        $newDbInfo = [];
        if (is_array($dbInfo)) {
            foreach ($dbInfo as $key => $value) {
                $newDbInfo[$key] = Crypt::decryptString($value);
            }
        }
        // dd($newDbInfo);
        return $newDbInfo;
    }
}
