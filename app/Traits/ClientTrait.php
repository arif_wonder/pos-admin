<?php
namespace App\Traits;

use App\Client;
use DB, Crypt, File, Config;

trait ClientTrait
{
    protected function createNewClientDBfolder(Client $user)
    {
        $SQLFile = File::get(base_path() . '/my_sql_file/raw-db.sql');
        $DB_HOST = env('DB_HOST');
        $DB_PORT = env('DB_PORT');
        $DB_USERNAME = env('DB_USERNAME');
        $DB_PASSWORD = env('DB_PASSWORD');
        $DB_DATABASE = env('DB_DATABASE');

        $userName = $user->username;
        $insertedId = $user->id;

        $createDBname = "pos_" . $insertedId . "_client";
        $createDBuser = env('DB_MASTER_USER');
        $createDBpass = env('DB_MASTER_PASS');
        $this->updateClientDBinfo($insertedId, $createDBuser, $createDBpass);

        if (
            DB::statement("CREATE DATABASE " . $createDBname) &&
            DB::statement("GRANT ALL PRIVILEGES ON " . $createDBname . ".* TO '" . $createDBuser . "'@'localhost'") &&
            DB::statement("FLUSH PRIVILEGES")
        ) {
            $connection = array();
            $connection['host'] = $DB_HOST;
            $connection['username'] = $createDBuser;
            $connection['password'] = $createDBpass;
            $connection['dbname'] = $createDBname;
            $connection['port'] = $DB_PORT;
            $this->setConfigWithConnection($createDBname, $connection);
            DB::unprepared($SQLFile);
            $passwd = md5(env('DEFAULT_USER_PASS'));
            $pref = 'a:4:{s:8:"language";s:7:"english";s:10:"base_color";s:5:"green";s:14:"pos_side_panel";s:5:"right";s:11:"pos_pattern";s:13:"brickwall.jpg";}';
            $insertAdminUserSql = "INSERT INTO `users` (`id`, `group_id`, `username`, `email`, `mobile`, `password`, `preference`) VALUES (1, 1, '" . $userName . "', '" . $user->email . "', '" . $user->mobile . "', '" . $passwd . "', '" . $pref . "')";
            DB::unprepared($insertAdminUserSql);

            // ********** end of the database **************** //
            $reConnection = array();
            $reConnection['host'] = $DB_HOST;
            $reConnection['username'] = $DB_USERNAME;
            $reConnection['password'] = $DB_PASSWORD;
            $reConnection['dbname'] = $DB_DATABASE;
            $reConnection['port'] = $DB_PORT;
            $this->setConfigWithConnection($DB_DATABASE, $reConnection);
            DB::statement("FLUSH PRIVILEGES");
            return DB::connection()->getDatabaseName();
        }
        return false;
    }

    protected function updateClientDBinfo($insertedId, $createDBuser, $pass)
    {
        $createDBname = "pos_" . $insertedId . "_client";
        $pass = env('DB_MASTER_PASS');
        $db_info = array(
            'host' => Crypt::encryptString(env('DB_HOST')),
            'port' => Crypt::encryptString(env('DB_PORT')),
            'db' => Crypt::encryptString($createDBname),
            'user' => Crypt::encryptString($createDBuser),
            'pass' => Crypt::encryptString($pass),
        );

        $db_info = base64_encode(serialize($db_info));

        $client = Client::find($insertedId);
        $client->db_info = $db_info;

        $client->save();
        return $insertedId;
    }

    public function setConfigWithConnection($dbname, $connection)
    {
        Config::set('database.connections.' . $dbname . '.driver', 'mysql');
        Config::set('database.connections.' . $dbname . '.charset', 'utf8mb4');
        Config::set('database.connections.' . $dbname . '.collation', 'utf8mb4_unicode_ci');
        Config::set('database.connections.' . $dbname . '.timezone', '+00:00');
        Config::set('database.connections.' . $dbname . '.host', $connection['host']);
        Config::set('database.connections.' . $dbname . '.username', $connection['username']);
        Config::set('database.connections.' . $dbname . '.password', $connection['password']);
        Config::set('database.connections.' . $dbname . '.database', $connection['dbname']);
        Config::set('database.connections.' . $dbname . '.port', $connection['port']);
        Config::set('database.connections.' . $dbname . '.strict', false);
        Config::set('database.default', $dbname);
        return DB::connection()->getDatabaseName();
	}
}
