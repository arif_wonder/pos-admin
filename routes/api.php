<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Authorization,Content-Type, Accept, x-xsrf-token");

/**
 * API Routes
 */

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::get('/user-mail', function() {
    $user = \App\UserLogin::find(1);
    return UserCreated($user);
});
Route::group(['namespace' => 'API'], function () {
    Route::post('/login', 'UserController@loginUser');
    Route::get('/countries','UserController@getCountries');
    Route::get('/business-types', 'ClientController@getBusinessTypes');
    Route::post('/add-store-admin', 'ClientController@addClient');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('/add-user', 'UserController@addUser');
        Route::post('/edit-user', 'UserController@editUser');
        Route::post('/delete-user', 'UserController@deleteUser');
        Route::post('/logout', 'UserController@logoutUser');
        Route::post('/password/change', 'UserController@changePassword');
        Route::post('/update-store-limit', 'UserController@updateStoreLimit');
        Route::post('/email-exists', 'UserController@checkEmailExists');
        Route::get('/mine/devices', 'ClientController@getClientDevices');
    });
});
