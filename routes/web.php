<?php
/* Route::get('/mailtest', function () {
    $client = App\Client::find(1);
    \Illuminate\Support\Facades\Mail::to($client->email)->send(new \App\Mail\ClientCreated($client));
});
 */
Route::get('/mailuser', function () {
    $client = App\UserLogin::find(1);
    return new \App\Mail\UserCreated($client, '123456');
});

// Auth::routes();
/************** AUTH START ******************/
Route::get('/', 'Auth\LoginController@showLoginForm')->name('home');
Route::get('/home', 'Auth\LoginController@showLoginForm');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
/************** AUTH END ******************/

Route::group(['namespace' => 'Admin', 'middleware' => ['auth', 'auth.admin']], function () {
    Route::get('/admin', 'HomeController@index')->name('admin.home');
    Route::any('/settings', 'SettingController@settings')->name('admin.setting');
    Route::get('/change-password', 'SettingController@index')->name('admin.password.form');
    Route::post('/change-password', 'SettingController@changePass')->name('admin.password.change');

    // Client group routes.
    Route::group(['prefix' => 'client'], function () {
        Route::get('/', ['as' => 'admin.client.home', 'uses' => 'ClientController@index']);

        Route::get('/add', 'ClientController@addClient')->name('admin.client.add');
        Route::post('/add-client', 'ClientController@addClientInformation')->name('admin.client.addclient');

        Route::get('/edit/{id}', 'ClientController@editClient')->name('admin.client.edit');
        Route::post('/edit-client/{id}', 'ClientController@updateClientInformation')->name('admin.client.editclient');

        Route::any('/status/{id}/{status}', 'ClientController@updateStatus')->name('admin.client.status');
    });
    Route::group(['prefix' => 'business-types'], function () {
        Route::get('/', 'BusinessTypeController@index')->name('admin.business.home');

        Route::any('/add', 'BusinessTypeController@addType')->name('admin.business.add');

        Route::any('/edit/{id}', 'BusinessTypeController@updateType')->name('admin.business.edit');
        Route::delete('/delete/', 'BusinessTypeController@deleteType')->name('admin.business.delete');

        Route::any('/status/{id}/{status}', 'BusinessTypeController@updateStatus')->name('admin.business.status');
    });
});
