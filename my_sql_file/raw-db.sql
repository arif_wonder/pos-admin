-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 14, 2019 at 10:31 AM
-- Server version: 5.7.25-0ubuntu0.16.04.2
-- PHP Version: 7.2.8-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos`
--

-- --------------------------------------------------------

--
-- Table structure for table `boxes`
--

CREATE TABLE `boxes` (
  `box_id` int(10) UNSIGNED NOT NULL,
  `box_name` varchar(100) NOT NULL,
  `box_details` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `boxes`
--

INSERT INTO `boxes` (`box_id`, `box_name`, `box_details`) VALUES
(1, 'Default Box', 'Default Box');

-- --------------------------------------------------------

--
-- Table structure for table `box_to_store`
--

CREATE TABLE `box_to_store` (
  `id` int(11) NOT NULL,
  `box_id` int(11) DEFAULT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bulk_messages`
--

CREATE TABLE `bulk_messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_phones` text,
  `message` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `buying_info`
--

CREATE TABLE `buying_info` (
  `info_id` int(10) UNSIGNED NOT NULL,
  `invoice_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `inv_type` enum('buy','expense','others','') NOT NULL DEFAULT 'buy',
  `store_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `total_item` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `status` enum('stock','using','','') NOT NULL DEFAULT 'stock',
  `total_sell` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `buy_date` date NOT NULL,
  `buy_time` time DEFAULT NULL,
  `sup_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `creator` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `invoice_note` text,
  `invoice_url` varchar(255) DEFAULT NULL,
  `is_visible` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `buying_item`
--

CREATE TABLE `buying_item` (
  `id` int(10) NOT NULL,
  `invoice_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `store_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `item_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) NOT NULL DEFAULT '0',
  `item_name` varchar(100) NOT NULL,
  `item_buying_price` float NOT NULL DEFAULT '0',
  `item_selling_price` float NOT NULL DEFAULT '0',
  `item_quantity` int(10) UNSIGNED DEFAULT NULL,
  `total_sell` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `defected_quantity` int(11) UNSIGNED DEFAULT '0',
  `status` enum('stock','active','sold','') NOT NULL DEFAULT 'stock',
  `item_total` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `buying_price`
--

CREATE TABLE `buying_price` (
  `price_id` int(10) NOT NULL,
  `invoice_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `store_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `paid_amount` float NOT NULL DEFAULT '0',
  `payment_mode` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `cart_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cart_products`
--

CREATE TABLE `cart_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(10) NOT NULL,
  `quantity` int(10) NOT NULL,
  `cart_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categorys`
--

CREATE TABLE `categorys` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(60) NOT NULL,
  `category_slug` varchar(60) NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `category_details` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category_suppliers`
--

CREATE TABLE `category_suppliers` (
  `category_id` int(11) NOT NULL,
  `sup_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category_to_store`
--

CREATE TABLE `category_to_store` (
  `c2s_id` int(10) NOT NULL,
  `ccategory_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_customers`
--

CREATE TABLE `coupon_customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) NOT NULL,
  `used_times` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` float(15,8) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `created_at`) VALUES
(1, 'Pound Sterling', 'GBP', '£', '', '2', 0.61250001, '2018-09-19 14:40:00'),
(2, 'US Dollar', 'USD', '$', '', '2', 1.00000000, '2018-09-19 14:40:00'),
(3, 'Euro', 'EUR', '', '€', '2', 0.78460002, '2018-09-19 14:40:00'),
(4, 'Hong Kong Dollar', 'HKD', 'HK$', '', '2', 7.82223988, '2018-09-19 12:00:00'),
(5, 'Indian Rupee', 'INR', '₹', '', '2', 64.40000153, '2018-09-19 12:00:00'),
(6, 'Russian Ruble', 'RUB', '₽', '', '2', 56.40359879, '2018-09-19 12:00:00'),
(7, 'Chinese Yuan Renminbi', 'CNY', '¥', '', '2', 6.34509993, '2018-09-19 12:00:00'),
(8, 'Australian Dollar', 'AUD', '$', '', '2', 1.26543999, '2018-09-19 12:00:00');
-- --------------------------------------------------------

--
-- Table structure for table `currency_to_store`
--

CREATE TABLE `currency_to_store` (
  `ca2s_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(10) UNSIGNED NOT NULL,
  `credit_balance` double(10,2) DEFAULT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `customer_email` varchar(100) DEFAULT NULL,
  `mobile_cc` varchar(10) NULL DEFAULT '91',
  `customer_mobile` varchar(14) DEFAULT NULL,
  `customer_address` text,
  `customer_sex` int(10) DEFAULT NULL,
  `customer_age` int(10) UNSIGNED DEFAULT NULL,
  `alt_contact_number` bigint(10) DEFAULT NULL,
  `passport_number` varchar(20) DEFAULT NULL,
  `dl_number` varchar(20) DEFAULT NULL,
  `pan_number` varchar(20) DEFAULT NULL,
  `aadhar_number` varchar(12) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `anniversary_date` date DEFAULT NULL,
  `occupation` int(11) DEFAULT NULL,
  `dob` varchar(100) DEFAULT NULL,
  `marital_status` enum('1','2','3','4','5') DEFAULT NULL COMMENT '1=married,2=widow,3=divorced,4=unmarried,5=other',
  `no_of_children` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_to_store`
--

CREATE TABLE `customer_to_store` (
  `c2s_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `currency_code` varchar(100) NOT NULL DEFAULT 'INR',
  `due_amount` float NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `disc_by_category`
--

CREATE TABLE `disc_by_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `discount` decimal(8,2) DEFAULT NULL,
  `discount_type` enum('fixed','precentage') NOT NULL DEFAULT 'fixed',
  `max_discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `min_purchase_amount` decimal(8,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `disc_coupons`
--

CREATE TABLE `disc_coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `code` varchar(50) NOT NULL,
  `valid_till` timestamp NULL DEFAULT NULL,
  `discount` decimal(8,2) NOT NULL,
  `discount_type` enum('fixed','precentage') NOT NULL DEFAULT 'fixed',
  `max_discount` decimal(10,2) DEFAULT '0.00',
  `min_purchase_amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `expired` tinyint(1) NOT NULL DEFAULT '0',
  `limit_per_user` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `disc_dpg_products`
--

CREATE TABLE `disc_dpg_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `dpg_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `disc_for_anniversary`
--

CREATE TABLE `disc_for_anniversary` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `discount` decimal(8,2) NOT NULL,
  `discount_type` enum('fixed','precentage') NOT NULL DEFAULT 'fixed',
  `min_purchase_amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `max_discount` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `disc_happy_hours`
--

CREATE TABLE `disc_happy_hours` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `time_from` time NOT NULL,
  `time_to` time NOT NULL,
  `discount` decimal(8,2) NOT NULL,
  `discount_type` enum('fixed','precentage') NOT NULL DEFAULT 'fixed',
  `max_discount` decimal(10,2) DEFAULT NULL,
  `min_purchase_amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `on_days` varchar(255) DEFAULT NULL COMMENT 'selected days in comma separated like - sun,mon, etc.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `disc_product_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `discount` decimal(8,2) NOT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `discount_type` enum('fixed','percentage') NOT NULL DEFAULT 'fixed',
  `max_discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `min_purchase_amount` decimal(8,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `family_members`
--

CREATE TABLE `family_members` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` enum('1','2','3') NOT NULL COMMENT '1=user,2=supplires,3=customer',
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `relationship` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `addedon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `discount` decimal(8,2) NOT NULL,
  `discount_type` enum('fixed','precentage') NOT NULL DEFAULT 'fixed',
  `max_discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `min_purchase_amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `form_date` date NOT NULL,
  `to_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `p2p_notif`
--

CREATE TABLE `p2p_notif` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `notif_id` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `payment_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `details` text,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_to_store`
--

CREATE TABLE `payment_to_store` (
  `p2s_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `printers`
--

CREATE TABLE `printers` (
  `printer_id` int(11) NOT NULL,
  `title` varchar(55) NOT NULL,
  `type` varchar(25) NOT NULL,
  `profile` varchar(25) NOT NULL,
  `char_per_line` tinyint(3) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `printer_to_store`
--

CREATE TABLE `printer_to_store` (
  `p2s_id` int(10) NOT NULL,
  `pprinter_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `path` varchar(255) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `port` varchar(10) DEFAULT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `p_id` int(10) UNSIGNED NOT NULL,
  `p_code` varchar(50) NOT NULL,
  `p_name` varchar(100) DEFAULT NULL,
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `p_image` varchar(250) DEFAULT NULL,
  `description` longtext,
  `p_brand` varchar(50) DEFAULT NULL,
  `p_quantity` int(11) DEFAULT NULL,
  `p_alert_qty` int(10) DEFAULT NULL,
  `p_unit` varchar(50) DEFAULT NULL,
  `p_taxrate` int(11) DEFAULT NULL,
  `p_regular_price` double(12,2) DEFAULT NULL,
  `p_special_price` double(12,2) DEFAULT NULL,
  `p_sp_fromdate` date DEFAULT NULL,
  `p_sp_todate` date DEFAULT NULL,
  `is_gift_item` TINYINT(1) NOT NULL DEFAULT '0' ,
  `p_gift_item` varchar(50) DEFAULT NULL,
  `p_bar_code` varchar(255) DEFAULT NULL,
  `p_purchage_price` double(12,2) DEFAULT NULL,
  `p_discount` decimal(8,2) DEFAULT '0.00',
  `p_discount_type` enum('fixed','precentage') NOT NULL DEFAULT 'fixed',
  `p_addedon` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_brands`
--

CREATE TABLE `product_brands` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `brand_description` text CHARACTER SET utf8,
  `addedon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_to_store`
--

CREATE TABLE `product_to_store` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `buy_price` float NOT NULL DEFAULT '0',
  `sell_price` float NOT NULL DEFAULT '0',
  `quantity_in_stock` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `sup_id` int(10) UNSIGNED NOT NULL,
  `box_id` int(11) DEFAULT NULL,
  `e_date` date DEFAULT NULL,
  `p_date` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `return_product_info`
--

CREATE TABLE `return_product_info` (
  `return_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `info_id` int(11) DEFAULT NULL,
  `invoice_id` varchar(100) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `comments` varchar(256) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `ret_amount` decimal(8,2) NOT NULL DEFAULT '0.00',
  `return_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `selling_info`
--

CREATE TABLE `selling_info` (
  `info_id` int(10) NOT NULL,
  `invoice_id` varchar(100) NOT NULL,
  `edit_counter` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `inv_type` enum('sell','due_paid','') NOT NULL DEFAULT 'sell',
  `store_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `customer_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `currency_code` varchar(3) NOT NULL DEFAULT 'INR',
  `payment_method` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `payment_mode` varchar(100) DEFAULT 'CASH',
  `ref_invoice_id` varchar(100) DEFAULT NULL,
  `ref_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `invoice_note` text,
  `return_note` text,
  `due_paid_note` text,
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  `is_due` tinyint(1) NOT NULL DEFAULT '0',
  `is_visible` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `return_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `short_url` varchar(255) DEFAULT NULL,
  `cart_data` text,
  `invoice_url` varchar(255) NULL DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `selling_item`
--

CREATE TABLE `selling_item` (
  `id` int(10) NOT NULL,
  `invoice_id` varchar(100) NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `sup_id` int(10) NOT NULL DEFAULT '0',
  `store_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `item_id` int(10) UNSIGNED NOT NULL,
  `item_name` varchar(100) NOT NULL,
  `item_price` float NOT NULL DEFAULT '0',
  `item_discount` float NOT NULL DEFAULT '0',
  `item_tax` float NOT NULL DEFAULT '0',
  `item_quantity` int(10) UNSIGNED NOT NULL,
  `total_buying_price` float NOT NULL DEFAULT '0',
  `item_total` float NOT NULL DEFAULT '0',
  `buying_invoice_id` varchar(100) DEFAULT NULL,
  `print_counter` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `print_counter_time` timestamp NULL DEFAULT NULL,
  `printed_by` int(10) NOT NULL DEFAULT '0',
  `returned` varchar(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `selling_price`
--

CREATE TABLE `selling_price` (
  `price_id` int(10) NOT NULL,
  `invoice_id` varchar(100) NOT NULL,
  `store_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `subtotal` float DEFAULT '0',
  `discount_type` enum('plain','percentage') NOT NULL DEFAULT 'plain',
  `discount_amount` float DEFAULT '0',
  `tax_amount` float NOT NULL DEFAULT '0',
  `previous_due` float NOT NULL DEFAULT '0',
  `payable_amount` float DEFAULT '0',
  `paid_amount` float NOT NULL DEFAULT '0',
  `paid_by_credit` decimal(8,2) NOT NULL DEFAULT '0.00',
  `todays_due` float NOT NULL DEFAULT '0',
  `present_due` float NOT NULL DEFAULT '0',
  `coupon_id` int(10) DEFAULT NULL,
  `payment_id` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) NOT NULL,
  `version` varchar(10) NOT NULL,
  `is_update_available` tinyint(1) NOT NULL DEFAULT '0',
  `update_version` varchar(100) DEFAULT NULL,
  `update_link` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`) VALUES
(1, 'Andhra Pradesh'),
(2, 'Arunachal Pradesh'),
(3, 'Assam'),
(4, 'Bihar'),
(5, 'Chhattisgarh'),
(6, 'Dadra and Nagar Haveli'),
(7, 'Daman and Diu'),
(8, 'Delhi'),
(9, 'Goa'),
(10, 'Gujarat'),
(11, 'Haryana'),
(12, 'Himachal Pradesh'),
(13, 'Jammu and Kashmir'),
(14, 'Jharkhand'),
(15, 'Karnataka'),
(16, 'Kerala'),
(17, 'Madhya Pradesh'),
(18, 'Maharashtra'),
(19, 'Manipur'),
(20, 'Meghalaya'),
(21, 'Mizoram'),
(22, 'Nagaland'),
(23, 'Orissa'),
(24, 'Puducherry'),
(25, 'Punjab'),
(26, 'Rajasthan'),
(27, 'Sikkim'),
(28, 'Tamil Nadu'),
(29, 'Telangana'),
(30, 'Tripura'),
(31, 'Uttar Pradesh'),
(32, 'Uttarakhand'),
(33, 'West Bengal');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `store_id` int(10) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `mobile` varchar(14) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `zip_code` varchar(50) DEFAULT NULL,
  `currency` varchar(100) NOT NULL DEFAULT 'USD',
  `vat_reg_no` varchar(250) DEFAULT NULL,
  `cashier_id` int(10) UNSIGNED DEFAULT NULL,
  `address` longtext,
  `area` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `receipt_printer` varchar(100) DEFAULT NULL,
  `cash_drawer_codes` varchar(100) DEFAULT NULL,
  `char_per_line` tinyint(4) NOT NULL DEFAULT '42',
  `remote_printing` tinyint(1) NOT NULL DEFAULT '1',
  `printer` int(11) DEFAULT NULL,
  `order_printers` varchar(100) DEFAULT NULL,
  `auto_print` tinyint(1) NOT NULL DEFAULT '0',
  `local_printers` tinyint(1) DEFAULT NULL,
  `logo` text,
  `favicon` varchar(250) DEFAULT NULL,
  `preference` longtext,
  `account_key` varchar(255) DEFAULT NULL,
  `wa_number` bigint(10) DEFAULT NULL,
  `sound_effect` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `sort_order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `feedback_status` varchar(100) NOT NULL DEFAULT 'ready',
  `feedback_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `gst_type` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `stores` (`store_id`, `name`, `mobile`, `country`, `currency`) VALUES
(1, 'DEFAULT STORE', '', 'India', 'INR');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `sup_id` int(10) UNSIGNED NOT NULL,
  `sup_name` varchar(100) NOT NULL,
  `sup_mobile` varchar(14) DEFAULT NULL,
  `sup_email` varchar(100) DEFAULT NULL,
  `sup_address` text,
  `sup_details` longtext,
  `sup_alternate_mobile` varchar(10) NOT NULL,
  `sup_aadhar_number` varchar(16) NOT NULL,
  `sup_pan_number` varchar(13) NOT NULL,
  `sup_driving_license` varchar(20) NOT NULL,
  `sup_passport_number` varchar(20) NOT NULL,
  `sup_city` varchar(20) NOT NULL,
  `sup_state` varchar(20) NOT NULL,
  `sup_zipcode` varchar(10) DEFAULT NULL,
  `sup_company_name` varchar(100) NOT NULL,
  `sup_company_address` text NOT NULL,
  `sup_company_contact_number` varchar(10) NOT NULL,
  `sup_company_tin_number` varchar(20) NOT NULL,
  `sup_company_tan_number` varchar(20) NOT NULL,
  `sup_company_gst_number` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_to_store`
--

CREATE TABLE `supplier_to_store` (
  `s2s_id` int(10) NOT NULL,
  `sup_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0',
  `balance` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tax_rates`
--

CREATE TABLE `tax_rates` (
  `tax_rate_id` int(11) NOT NULL,
  `rate` decimal(10,2) DEFAULT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `addedon` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unites`
--

CREATE TABLE `unites` (
  `unit_id` int(11) NOT NULL,
  `title` varchar(10) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `addedon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `raw_password` varchar(100) DEFAULT NULL,
  `pass_reset_code` varchar(32) DEFAULT NULL,
  `reset_code_time` datetime DEFAULT NULL,
  `ip` varchar(40) DEFAULT NULL,
  `preference` longtext DEFAULT NULL,
  `api_token` text,
  `device_id` varchar(100) DEFAULT NULL,
  `device_type` VARCHAR(50) NULL DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users_bank`
--

CREATE TABLE `users_bank` (
  `bank_id` int(11) NOT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `bank_acc_number` varchar(20) DEFAULT NULL,
  `ifsc_code` varchar(10) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_type` enum('1','2','3') NOT NULL DEFAULT '1' COMMENT '1=user,2=suppliers,3=customer',
  `addedon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `user_details_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_type` enum('1','2','3') NOT NULL DEFAULT '1' COMMENT '1=user,2=supplires,3=customer',
  `vehicle_number` varchar(4) DEFAULT NULL,
  `vehicle_model` varchar(10) DEFAULT NULL,
  `number_of_vehicles` int(11) DEFAULT NULL,
  `busi_name` varchar(50) DEFAULT NULL,
  `busi_address` text,
  `busi_contact_number` varchar(10) DEFAULT NULL,
  `busi_annual_turnover` double(15,2) DEFAULT '0.00',
  `busi_trans_to_office` varchar(100) DEFAULT NULL,
  `ser_man_designation` varchar(10) DEFAULT NULL,
  `ser_man_comp_name` varchar(100) DEFAULT NULL,
  `ser_man_comp_address` text,
  `ser_man_comp_cont_number` varchar(10) DEFAULT NULL,
  `ser_man_trans_to_office` varchar(100) DEFAULT NULL,
  `com_designation` varchar(50) DEFAULT NULL,
  `perma_address` text,
  `perma_city` varchar(50) DEFAULT NULL,
  `perma_state` varchar(50) DEFAULT NULL,
  `perma_zip_code` int(6) DEFAULT NULL,
  `cor_address` text,
  `cor_city` varchar(50) DEFAULT NULL,
  `cor_state` varchar(50) DEFAULT NULL,
  `cor_zip_code` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `slug` varchar(100) CHARACTER SET utf16 NOT NULL,
  `sort_order` int(10) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `permission` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`group_id`, `name`, `slug`, `sort_order`, `status`, `permission`) VALUES
(1, 'Admin', 'admin', 1, 1, 'a:1:{s:6:"access";a:88:{s:16:"read_sell_report";s:4:"true";s:20:"read_overview_report";s:4:"true";s:22:"read_collection_report";s:4:"true";s:27:"read_full_collection_report";s:4:"true";s:26:"read_due_collection_report";s:4:"true";s:13:"read_analysis";s:4:"true";s:21:"send_report_via_email";s:4:"true";s:15:"read_buy_report";s:4:"true";s:19:"read_payment_report";s:4:"true";s:17:"read_stock_report";s:4:"true";s:17:"send_report_email";s:4:"true";s:14:"create_invoice";s:4:"true";s:17:"read_invoice_list";s:4:"true";s:12:"view_invoice";s:4:"true";s:14:"update_invoice";s:4:"true";s:19:"add_item_to_invoice";s:4:"true";s:24:"remove_item_from_invoice";s:4:"true";s:14:"delete_invoice";s:4:"true";s:13:"email_invoice";s:4:"true";s:10:"create_due";s:4:"true";s:14:"due_collection";s:4:"true";s:12:"read_product";s:4:"true";s:14:"create_product";s:4:"true";s:14:"update_product";s:4:"true";s:14:"delete_product";s:4:"true";s:14:"import_product";s:4:"true";s:19:"product_bulk_action";s:4:"true";s:18:"delete_all_product";s:4:"true";s:13:"read_category";s:4:"true";s:15:"create_category";s:4:"true";s:15:"update_category";s:4:"true";s:15:"delete_category";s:4:"true";s:16:"read_stock_alert";s:4:"true";s:20:"read_expired_product";s:4:"true";s:13:"print_barcode";s:4:"true";s:19:"restore_all_product";s:4:"true";s:13:"read_supplier";s:4:"true";s:15:"create_supplier";s:4:"true";s:15:"update_supplier";s:4:"true";s:15:"delete_supplier";s:4:"true";s:21:"read_supplier_profile";s:4:"true";s:8:"read_box";s:4:"true";s:10:"create_box";s:4:"true";s:10:"update_box";s:4:"true";s:10:"delete_box";s:4:"true";s:21:"create_buying_invoice";s:4:"true";s:21:"update_buying_invoice";s:4:"true";s:21:"delete_buying_invoice";s:4:"true";s:14:"product_return";s:4:"true";s:12:"read_expense";s:4:"true";s:14:"create_expense";s:4:"true";s:14:"update_expense";s:4:"true";s:14:"delete_expense";s:4:"true";s:13:"read_customer";s:4:"true";s:21:"read_customer_profile";s:4:"true";s:15:"create_customer";s:4:"true";s:15:"update_customer";s:4:"true";s:15:"delete_customer";s:4:"true";s:9:"read_user";s:4:"true";s:11:"create_user";s:4:"true";s:11:"update_user";s:4:"true";s:11:"delete_user";s:4:"true";s:15:"change_password";s:4:"true";s:14:"read_usergroup";s:4:"true";s:16:"create_usergroup";s:4:"true";s:16:"update_usergroup";s:4:"true";s:16:"delete_usergroup";s:4:"true";s:13:"read_currency";s:4:"true";s:15:"create_currency";s:4:"true";s:15:"update_currency";s:4:"true";s:15:"change_currency";s:4:"true";s:15:"delete_currency";s:4:"true";s:16:"read_filemanager";s:4:"true";s:19:"read_payment_method";s:4:"true";s:21:"create_payment_method";s:4:"true";s:21:"update_payment_method";s:4:"true";s:21:"delete_payment_method";s:4:"true";s:10:"read_store";s:4:"true";s:12:"create_store";s:4:"true";s:12:"update_store";s:4:"true";s:12:"delete_store";s:4:"true";s:14:"activate_store";s:4:"true";s:14:"upload_favicon";s:4:"true";s:11:"upload_logo";s:4:"true";s:20:"read_user_preference";s:4:"true";s:22:"update_user_preference";s:4:"true";s:9:"filtering";s:4:"true";s:22:"read_keyboard_shortcut";s:4:"true";}}');

-- --------------------------------------------------------

--
-- Table structure for table `user_to_store`
--

CREATE TABLE `user_to_store` (
  `u2s_id` int(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user_to_store` (`u2s_id`, `user_id`, `store_id`, `status`, `sort_order`) VALUES (1, 1, 1, 1, 0);
--
-- Indexes for dumped tables
--

--
-- Indexes for table `boxes`
--
ALTER TABLE `boxes`
  ADD PRIMARY KEY (`box_id`);

--
-- Indexes for table `box_to_store`
--
ALTER TABLE `box_to_store`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bulk_messages`
--
ALTER TABLE `bulk_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buying_info`
--
ALTER TABLE `buying_info`
  ADD PRIMARY KEY (`info_id`);

--
-- Indexes for table `buying_item`
--
ALTER TABLE `buying_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buying_price`
--
ALTER TABLE `buying_price`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `cart_products`
--
ALTER TABLE `cart_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorys`
--
ALTER TABLE `categorys`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `category_to_store`
--
ALTER TABLE `category_to_store`
  ADD PRIMARY KEY (`c2s_id`);

--
-- Indexes for table `coupon_customers`
--
ALTER TABLE `coupon_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `currency_to_store`
--
ALTER TABLE `currency_to_store`
  ADD PRIMARY KEY (`ca2s_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `customer_to_store`
--
ALTER TABLE `customer_to_store`
  ADD PRIMARY KEY (`c2s_id`);

--
-- Indexes for table `disc_by_category`
--
ALTER TABLE `disc_by_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disc_coupons`
--
ALTER TABLE `disc_coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disc_dpg_products`
--
ALTER TABLE `disc_dpg_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disc_for_anniversary`
--
ALTER TABLE `disc_for_anniversary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disc_happy_hours`
--
ALTER TABLE `disc_happy_hours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disc_product_group`
--
ALTER TABLE `disc_product_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `family_members`
--
ALTER TABLE `family_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p2p_notif`
--
ALTER TABLE `p2p_notif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `payment_to_store`
--
ALTER TABLE `payment_to_store`
  ADD PRIMARY KEY (`p2s_id`);

--
-- Indexes for table `printers`
--
ALTER TABLE `printers`
  ADD PRIMARY KEY (`printer_id`);

--
-- Indexes for table `printer_to_store`
--
ALTER TABLE `printer_to_store`
  ADD PRIMARY KEY (`p2s_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`p_id`),
  ADD UNIQUE KEY `p_code` (`p_code`) USING BTREE;

--
-- Indexes for table `product_brands`
--
ALTER TABLE `product_brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `product_to_store`
--
ALTER TABLE `product_to_store`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `return_product_info`
--
ALTER TABLE `return_product_info`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `selling_info`
--
ALTER TABLE `selling_info`
  ADD PRIMARY KEY (`info_id`);

--
-- Indexes for table `selling_item`
--
ALTER TABLE `selling_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `selling_price`
--
ALTER TABLE `selling_price`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `supplier_to_store`
--
ALTER TABLE `supplier_to_store`
  ADD PRIMARY KEY (`s2s_id`);

--
-- Indexes for table `tax_rates`
--
ALTER TABLE `tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Indexes for table `unites`
--
ALTER TABLE `unites`
  ADD PRIMARY KEY (`unit_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_bank`
--
ALTER TABLE `users_bank`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`user_details_id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`group_id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `user_to_store`
--
ALTER TABLE `user_to_store`
  ADD PRIMARY KEY (`u2s_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boxes`
--
ALTER TABLE `boxes`
  MODIFY `box_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `box_to_store`
--
ALTER TABLE `box_to_store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bulk_messages`
--
ALTER TABLE `bulk_messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `buying_info`
--
ALTER TABLE `buying_info`
  MODIFY `info_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `buying_item`
--
ALTER TABLE `buying_item`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `buying_price`
--
ALTER TABLE `buying_price`
  MODIFY `price_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cart_products`
--
ALTER TABLE `cart_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categorys`
--
ALTER TABLE `categorys`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category_to_store`
--
ALTER TABLE `category_to_store`
  MODIFY `c2s_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coupon_customers`
--
ALTER TABLE `coupon_customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `currency_to_store`
--
ALTER TABLE `currency_to_store`
  MODIFY `ca2s_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_to_store`
--
ALTER TABLE `customer_to_store`
  MODIFY `c2s_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `disc_by_category`
--
ALTER TABLE `disc_by_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `disc_coupons`
--
ALTER TABLE `disc_coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `disc_for_anniversary`
--
ALTER TABLE `disc_for_anniversary`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `disc_happy_hours`
--
ALTER TABLE `disc_happy_hours`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `family_members`
--
ALTER TABLE `family_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `p2p_notif`
--
ALTER TABLE `p2p_notif`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `payment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_to_store`
--
ALTER TABLE `payment_to_store`
  MODIFY `p2s_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `printers`
--
ALTER TABLE `printers`
  MODIFY `printer_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `printer_to_store`
--
ALTER TABLE `printer_to_store`
  MODIFY `p2s_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `p_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_brands`
--
ALTER TABLE `product_brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_to_store`
--
ALTER TABLE `product_to_store`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `return_product_info`
--
ALTER TABLE `return_product_info`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `selling_info`
--
ALTER TABLE `selling_info`
  MODIFY `info_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `selling_item`
--
ALTER TABLE `selling_item`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `selling_price`
--
ALTER TABLE `selling_price`
  MODIFY `price_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `store_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `sup_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `supplier_to_store`
--
ALTER TABLE `supplier_to_store`
  MODIFY `s2s_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tax_rates`
--
ALTER TABLE `tax_rates`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `unites`
--
ALTER TABLE `unites`
  MODIFY `unit_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users_bank`
--
ALTER TABLE `users_bank`
  MODIFY `bank_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `user_details_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_to_store`
--
ALTER TABLE `user_to_store`
  MODIFY `u2s_id` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- ----------------------------------
-- CHANGING TABLE/COLUMN DEFINITIONS
-- ----------------------------------

ALTER TABLE `box_to_store` CHANGE `box_id` `box_id` INT(10) UNSIGNED NULL DEFAULT NULL;
ALTER TABLE `stores` CHANGE `store_id` `store_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `box_to_store` CHANGE `store_id` `store_id` INT(10) UNSIGNED NOT NULL DEFAULT '1';

ALTER TABLE `buying_info` CHANGE `status` `status` ENUM('stock','using') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'stock';

ALTER TABLE `buying_item` CHANGE `category_id` `category_id` INT(10) UNSIGNED NOT NULL DEFAULT '0';

ALTER TABLE `carts` CHANGE `user_id` `user_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `cart_products` CHANGE `product_id` `product_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `category_suppliers` CHANGE `category_id` `category_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `category_suppliers` CHANGE `sup_id` `sup_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `category_suppliers` CHANGE `store_id` `store_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `category_to_store` CHANGE `ccategory_id` `ccategory_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `category_to_store` CHANGE `store_id` `store_id` INT(10) UNSIGNED NOT NULL DEFAULT '1';

ALTER TABLE `coupon_customers` CHANGE `customer_id` `customer_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `currency_to_store` CHANGE `store_id` `store_id` INT(10) UNSIGNED NOT NULL DEFAULT '1';

ALTER TABLE `customer_to_store` CHANGE `customer_id` `customer_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `customer_to_store` CHANGE `store_id` `store_id` INT(10) UNSIGNED NOT NULL DEFAULT '1';

ALTER TABLE `family_members` CHANGE `user_id` `user_id` INT(10) UNSIGNED NOT NULL;

ALTER TABLE `payment_to_store` CHANGE `payment_id` `payment_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `payment_to_store` CHANGE `store_id` `store_id` INT(10) UNSIGNED NOT NULL DEFAULT '1';

ALTER TABLE `printer_to_store` CHANGE `store_id` `store_id` INT(10) UNSIGNED NOT NULL DEFAULT '1';

ALTER TABLE `product_to_store` CHANGE `product_id` `product_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `product_to_store` CHANGE `store_id` `store_id` INT(10) UNSIGNED NOT NULL DEFAULT '1';
ALTER TABLE `product_to_store` CHANGE `box_id` `box_id` INT(10) UNSIGNED NULL DEFAULT NULL;

ALTER TABLE `return_product_info` CHANGE `invoice_id` `invoice_id` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `return_product_info` CHANGE `product_id` `product_id` INT(10) UNSIGNED NULL DEFAULT NULL;
ALTER TABLE `return_product_info` CHANGE `customer_id` `customer_id` INT(10) UNSIGNED NULL DEFAULT NULL;
ALTER TABLE `return_product_info` CHANGE `user_id` `user_id` INT(10) UNSIGNED NULL DEFAULT NULL;
ALTER TABLE `return_product_info` CHANGE `store_id` `store_id` INT(10) UNSIGNED NULL DEFAULT NULL;

ALTER TABLE `selling_item` CHANGE `sup_id` `sup_id` INT(10) UNSIGNED NOT NULL DEFAULT '0';

ALTER TABLE `selling_item` CHANGE `buying_invoice_id` `buying_invoice_id` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `selling_price` CHANGE `coupon_id` `coupon_id` INT(10) UNSIGNED NULL DEFAULT NULL;

ALTER TABLE `supplier_to_store` CHANGE `sup_id` `sup_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `supplier_to_store` CHANGE `store_id` `store_id` INT(10) UNSIGNED NOT NULL DEFAULT '1';

ALTER TABLE `users_bank` CHANGE `user_id` `user_id` INT(10) UNSIGNED NULL DEFAULT NULL;

ALTER TABLE `user_details` CHANGE `user_id` `user_id` INT(10) UNSIGNED NULL DEFAULT NULL;

ALTER TABLE `user_to_store` CHANGE `user_id` `user_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `user_to_store` CHANGE `store_id` `store_id` INT(10) UNSIGNED NOT NULL DEFAULT '1';

-- ----------------------------------
-- DEFINING CONSTRAINTS AND INDEXES
-- ----------------------------------

SET FOREIGN_KEY_CHECKS=0;
-- Table box_to_store
ALTER TABLE `box_to_store` ADD CONSTRAINT `fk_bts` FOREIGN KEY (`box_id`) REFERENCES `boxes`(`box_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `box_to_store` ADD CONSTRAINT `fk_bx_st_id` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Table buying_info
ALTER TABLE `buying_info` ADD CONSTRAINT `fk_bi_st_id` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON UPDATE CASCADE;
ALTER TABLE `buying_info` ADD CONSTRAINT `fk_bi_sup_id` FOREIGN KEY (`sup_id`) REFERENCES `suppliers`(`sup_id`) ON UPDATE CASCADE;
ALTER TABLE `buying_info` ADD CONSTRAINT `fk_bi_uid` FOREIGN KEY (`creator`) REFERENCES `users`(`id`) ON UPDATE CASCADE;
ALTER TABLE `buying_info` ADD INDEX(`status`);
ALTER TABLE `buying_info` ADD INDEX(`invoice_id`);

-- Table buying_item
ALTER TABLE `buying_item` ADD INDEX(`invoice_id`);
ALTER TABLE `buying_item` ADD CONSTRAINT `fk_bitm_st_id` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON UPDATE CASCADE;
ALTER TABLE `buying_item` ADD CONSTRAINT `fk_bitm_inv_id` FOREIGN KEY (`invoice_id`) REFERENCES `buying_info`(`invoice_id`) ON UPDATE CASCADE;
ALTER TABLE `buying_item` ADD CONSTRAINT `fk_bitm_itm_id` FOREIGN KEY (`item_id`) REFERENCES `products`(`p_id`) ON UPDATE CASCADE ON DELETE SET NULL;

-- Table buying_price
ALTER TABLE `buying_price` ADD CONSTRAINT `fk_bp_st_id` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON UPDATE CASCADE;
ALTER TABLE `buying_price` ADD CONSTRAINT `fk_bp_inv_id` FOREIGN KEY (`invoice_id`) REFERENCES `buying_info`(`invoice_id`) ON UPDATE CASCADE;

-- Table carts
ALTER TABLE `carts` ADD CONSTRAINT `fk_cart_uid` FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON UPDATE CASCADE;

-- Table cart_products
ALTER TABLE `cart_products` ADD CONSTRAINT `fk_cp_cartid` FOREIGN KEY (`cart_id`) REFERENCES `carts`(`cart_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `cart_products` ADD CONSTRAINT `fk_cp_prdid` FOREIGN KEY (`product_id`) REFERENCES `products`(`p_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Table categorys
ALTER TABLE `categorys` ADD INDEX(`category_name`);

-- Table category_suppliers
ALTER TABLE `category_suppliers` ADD CONSTRAINT `fk_csup_catid` FOREIGN KEY (`category_id`) REFERENCES `categorys`(`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `category_suppliers` ADD CONSTRAINT `fk_csup_supid` FOREIGN KEY (`sup_id`) REFERENCES `suppliers`(`sup_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `category_suppliers` ADD CONSTRAINT `fk_csup_strid` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Table category_to_store
ALTER TABLE `category_to_store` ADD CONSTRAINT `fk_cts_catid` FOREIGN KEY (`ccategory_id`) REFERENCES `categorys`(`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `category_to_store` ADD CONSTRAINT `fk_cts_strid` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Table coupon_customers
ALTER TABLE `coupon_customers` ADD CONSTRAINT `fk_cpcus_cpid` FOREIGN KEY (`coupon_id`) REFERENCES `disc_coupons`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `coupon_customers` ADD CONSTRAINT `fk_cpcus_cusid` FOREIGN KEY (`customer_id`) REFERENCES `customers`(`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Table currency_to_store
ALTER TABLE `currency_to_store` ADD CONSTRAINT `fk_c2s_curid` FOREIGN KEY (`currency_id`) REFERENCES `currency`(`currency_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `currency_to_store` ADD CONSTRAINT `fk_c2s_strid` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Table customers
ALTER TABLE `customers` ADD INDEX(`customer_name`);
ALTER TABLE `customers` ADD INDEX(`customer_email`);
ALTER TABLE `customers` ADD INDEX(`customer_mobile`);

-- Table customer_to_store
ALTER TABLE `customer_to_store` ADD CONSTRAINT `fk_cus2s_cusid` FOREIGN KEY (`customer_id`) REFERENCES `customers`(`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `customer_to_store` ADD CONSTRAINT `fk_cus2s_strid` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Table disc_by_category
ALTER TABLE `disc_by_category` ADD CONSTRAINT `fk_disbycat_catid` FOREIGN KEY (`category_id`) REFERENCES `categorys`(`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Table family_members
ALTER TABLE `family_members` ADD CONSTRAINT `fk_fam_mem_uid` FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `p2p_notif` ADD UNIQUE(`notif_id`);
ALTER TABLE `payments` ADD INDEX(`name`);

-- Table payment_to_store
ALTER TABLE `payment_to_store` ADD CONSTRAINT `fk_p2s_payid` FOREIGN KEY (`payment_id`) REFERENCES `payments`(`payment_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `payment_to_store` ADD CONSTRAINT `fk_p2s_strid` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Table printer_to_store
ALTER TABLE `printer_to_store` ADD CONSTRAINT `fk_pr2st_payid` FOREIGN KEY (`pprinter_id`) REFERENCES `printers`(`printer_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `printer_to_store` ADD CONSTRAINT `fk_pr2st_strid` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Table products
ALTER TABLE `products` ADD INDEX(`p_name`);
ALTER TABLE `products` ADD CONSTRAINT `fk_prdct_catid` FOREIGN KEY (`category_id`) REFERENCES `categorys`(`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Table product_brands
ALTER TABLE `product_brands` ADD INDEX(`brand_name`);

-- Table product_to_store
ALTER TABLE `product_to_store` ADD CONSTRAINT `fk_prd2st_prdid` FOREIGN KEY (`product_id`) REFERENCES `products`(`p_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `product_to_store` ADD CONSTRAINT `fk_prd2st_strid` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `product_to_store` ADD CONSTRAINT `fk_prd2st_boxid` FOREIGN KEY (`box_id`) REFERENCES `boxes`(`box_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*-- Table selling_info
ALTER TABLE `selling_info` ADD UNIQUE(`invoice_id`);
ALTER TABLE `selling_info` ADD CONSTRAINT `fk_slin_strid` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `selling_info` ADD CONSTRAINT `fk_slin_custid` FOREIGN KEY (`customer_id`) REFERENCES `customers`(`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `selling_info` ADD CONSTRAINT `fk_slin_crtduid` FOREIGN KEY (`created_by`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Table selling_item
ALTER TABLE `selling_item` ADD CONSTRAINT `fk_slitm_invid` FOREIGN KEY (`invoice_id`) REFERENCES `selling_info`(`invoice_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `selling_item` ADD CONSTRAINT `fk_slitm_catid` FOREIGN KEY (`category_id`) REFERENCES `categorys`(`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `selling_item` ADD CONSTRAINT `fk_slitm_supid` FOREIGN KEY (`sup_id`) REFERENCES `suppliers`(`sup_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `selling_item` ADD CONSTRAINT `fk_slitm_strid` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `selling_item` ADD CONSTRAINT `fk_slitm_itmid` FOREIGN KEY (`item_id`) REFERENCES `products`(`p_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `selling_item` ADD CONSTRAINT `fk_slitm_byinvid` FOREIGN KEY (`buying_invoice_id`) REFERENCES `buying_info`(`invoice_id`) ON DELETE SET NULL ON UPDATE SET NULL;

-- Table selling_price
ALTER TABLE `selling_price` ADD CONSTRAINT `fk_slprc_invid` FOREIGN KEY (`invoice_id`) REFERENCES `selling_info`(`invoice_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `selling_price` ADD CONSTRAINT `fk_slprc_strid` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `selling_price` ADD CONSTRAINT `fk_slprc_cpnid` FOREIGN KEY (`coupon_id`) REFERENCES `disc_coupons`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
*/
-- Table return_product_info
ALTER TABLE `return_product_info` ADD CONSTRAINT `fk_retprd_infid` FOREIGN KEY (`info_id`) REFERENCES `selling_item`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `return_product_info` ADD CONSTRAINT `fk_retprd_invid` FOREIGN KEY (`invoice_id`) REFERENCES `selling_info`(`invoice_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `return_product_info` ADD CONSTRAINT `fk_retprd_prdid` FOREIGN KEY (`product_id`) REFERENCES `products`(`p_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `return_product_info` ADD CONSTRAINT `fk_retprd_custid` FOREIGN KEY (`customer_id`) REFERENCES `customers`(`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `return_product_info` ADD CONSTRAINT `fk_retprd_uid` FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `return_product_info` ADD CONSTRAINT `fk_retprd_strid` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Table stores
ALTER TABLE `stores` ADD INDEX(`name`);

-- Table suppliers
ALTER TABLE `suppliers` ADD INDEX(`sup_name`);
ALTER TABLE `suppliers` ADD INDEX(`sup_mobile`);

-- Table supplier_to_store
ALTER TABLE `supplier_to_store` ADD CONSTRAINT `fk_sup2str_supid` FOREIGN KEY (`sup_id`) REFERENCES `suppliers`(`sup_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `supplier_to_store` ADD CONSTRAINT `fk_sup2str_strid` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Table users
ALTER TABLE `users` ADD CONSTRAINT `fk_usr_grpid` FOREIGN KEY (`group_id`) REFERENCES `user_group`(`group_id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `users` ADD INDEX(`username`);
ALTER TABLE `users` ADD INDEX(`email`);
ALTER TABLE `users` ADD INDEX(`mobile`);

-- Table users_bank
ALTER TABLE `users_bank` ADD INDEX(`bank_acc_number`);

-- Table user_to_store
ALTER TABLE `user_to_store` ADD CONSTRAINT `fk_u2s_uid` FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `user_to_store` ADD CONSTRAINT `fk_u2s_strid` FOREIGN KEY (`store_id`) REFERENCES `stores`(`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;
