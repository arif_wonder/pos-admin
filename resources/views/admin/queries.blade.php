@extends('layouts.admin')
@section('title', 'Queries')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Queries</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="data-table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>S.N.</th>
                        <th>Name</th>
                        <th>E-mail ID</th>
                        <th>Mobile</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($queries))
                        @foreach ($queries as $query)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $query->name }}</td>
                                <td>{{ $query->email }}</td>
                                <td>{{ $query->mobile }}</td>
                                <td>{{ $query->status }}</td>
                                <td>
                                    <a href="#" data-cmt-url="{{ route('admin.queries.comment', ['id'=>$query->contact_id]) }}" data-toggle="tooltip" title="Click to view comment" class="btn btn-info btn-flat btn-xs get-cmt"><i class="fa fa-comment"></i> View</a>
                                    @if ($query->status == 'Open')
                                        <a href="{{ route('admin.queries.status', ['id'=>$query->contact_id, 'status' => 'Resolved']) }}" data-toggle="tooltip" title="Click to resolve" class="btn btn-success btn-flat btn-xs"><i class="fa fa-lock"></i> Resolve</a>
                                        <a href="{{ route('admin.queries.status', ['id'=>$query->contact_id, 'status' => 'Closed']) }}" data-toggle="tooltip" title="Click to close query" class="btn btn-success btn-flat btn-xs"><i class="fa fa-lock"></i> Close</a>
                                    @else
                                        <a href="{{ route('admin.queries.status', ['id'=>$query->contact_id, 'status' => 'Open']) }}" data-toggle="tooltip" title="Click to re-open" class="btn btn-warning btn-flat btn-xs"><i class="fa fa-unlock"></i> Re-Open</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <th>S.N.</th>
                        <th>Name</th>
                        <th>E-mail ID</th>
                        <th>Mobile</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    
    <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Modal Title</h4>
              </div>
              <div class="modal-body"></div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        
@endsection
@section('styles')
    <link rel="stylesheet" href="{{ asset('plugins/datatables/css/dataTables.bootstrap.min.css') }}" />
@endsection
@section('scripts')
    <script src="{{ asset('plugins/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/js/dataTables.bootstrap.min.js') }}"></script>
    <script>
    $(function () {
        $('.get-cmt').click(function(e){
            e.preventDefault();
            var c_url = $(this).data('cmt-url');
            $.ajax({
               type:'GET',
               url: c_url,
               dataType: 'JSON',
               success:function(data){
                  if (data.query) {
                      $('#modal-default .modal-body').html(data.query.comment);
                      $('#modal-default .modal-title').html(data.query.name);
                      $('#modal-default').modal('show');
                  }
               }
            });
        });
        
        $('#data-table').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false,
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [5] }
            ]
        });
    });
    </script>
@endsection
