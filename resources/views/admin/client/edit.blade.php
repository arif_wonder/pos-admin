@extends('layouts.admin')
@section('title', 'Edit Store Admin')

@section('content')
@php
$genders = ['male' => 'Male', 'female' => 'Female'];
@endphp

<div class="box box-success box-12">
    <div class="box-header with-border">
        <h3 class="box-title">Edit Store Admin</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" method="post" action="{{ route('admin.client.editclient', ['id' => $client->id]) }}">
        {{ csrf_field() }}
        <div class="box-body">
            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('business_type_id') ? ' has-error' : '' }}">
                <label for="mobile" class="col-sm-3 control-label required">Business Type</label>
                <div class="col-sm-9">
                    <select name="business_type_id" class="select2">
                        <option value="">Select</option>
                        @foreach ($businessTypes as $type)
                        <option {{ (old('business_type_id') == $type->id || $client->business_type_id == $type->id) ? 'selected' : '' }} value="{{ $type->id }}">{{ $type->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('business_type_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('business_type_id') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-sm-3 control-label required">Name</label>
                <div class="col-sm-9">
                    <input type="text" value="{{ old('name') ?: $client->name }}" name="name" class="form-control" id="name" placeholder="Full Name">
                    @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('username') ? ' has-error' : '' }}">
                <label for="username" class="col-sm-3 control-label required">Username</label>
                <div class="col-sm-9">
                    <input type="text" disabled="true" value="{{ old('username') ?: $client->username }}" name="username" class="string form-control" id="username" placeholder="Username">
                    @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-sm-3 control-label required">E-mail ID</label>
                <div class="col-sm-9">
                    <input type="email" value="{{ old('email') ?: $client->email }}" name="email" class="form-control" id="email" placeholder="abc@example.com">
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('max_stores') ? ' has-error' : '' }}">
                <label for="max_stores" class="col-sm-3 control-label required">Max Stores</label>
                <div class="col-sm-9">
                    <input type="text" value="{{ old('max_stores') ?: $client->max_stores }}" name="max_stores" class="number form-control" id="max_stores" placeholder="Max Stores(Like - 10)">
                    @if ($errors->has('max_stores'))
                    <span class="help-block">
                        <strong>{{ $errors->first('max_stores') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group col-sm-10 col-sm-offset-1{{ ($errors->has('mobile') || $errors->has('mobile_cc')) ? ' has-error' : '' }}">
                <label for="mobile" class="col-sm-3 control-label required">Mobile Number</label>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-6">
                            <select name="mobile_cc" class="select2">
                                <option value="">Country</option>
                                @foreach ($countries as $country)
                                <option {{ (old('mobile_cc') == $country->code || $client->mobile_cc == $country->code) ? 'selected' : '' }} value="{{ $country->code }}">{{ $country->name }}({{ $country->code }})</option>
                                @endforeach
                            </select>
                            @if ($errors->has('mobile_cc'))
                            <span class="help-block">
                                <strong>{{ $errors->first('mobile_cc') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-sm-6">
                            <input type="text" value="{{ old('mobile') ?: $client->mobile }}" name="mobile" class="number form-control" id="mobile" placeholder="Mobile Number">
                            @if ($errors->has('mobile'))
                            <span class="help-block">
                                <strong>{{ $errors->first('mobile') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('company_name') ? ' has-error' : '' }}">
                <label for="company_name" class="col-sm-3 control-label required">Company Name</label>
                <div class="col-sm-9">
                    <input type="company_name" value="{{ old('company_name') ?: $client->company }}" name="company_name" class="form-control" id="company_name" placeholder="Company Name">
                    @if ($errors->has('company_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('company_name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('address') ? ' has-error' : '' }}">
                <label for="address" class="col-sm-3 control-label required">Address</label>
                <div class="col-sm-9">
                    <input type="text" value="{{ old('address') ?: $client->address }}" name="address" class="form-control" id="address" placeholder="address">
                    @if ($errors->has('mobile'))
                    <span class="help-block">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('area') ? ' has-error' : '' }}">
                <label for="area" class="col-sm-3 control-label">Area</label>
                <div class="col-sm-9">
                    <input type="text" value="{{ old('area') ?: $client->area }}" name="area" class="form-control" id="area" placeholder="Area">
                    @if ($errors->has('area'))
                    <span class="help-block">
                        <strong>{{ $errors->first('area') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('city') ? ' has-error' : '' }}">
                <label for="city" class="col-sm-3 control-label">City</label>
                <div class="col-sm-9">
                    <input type="text" value="{{ old('city') ?: $client->city }}" name="city" class="form-control" id="city" placeholder="City">
                    @if ($errors->has('city'))
                    <span class="help-block">
                        <strong>{{ $errors->first('city') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('district') ? ' has-error' : '' }}">
                <label for="district" class="col-sm-3 control-label">District</label>
                <div class="col-sm-9">
                    <input type="text" value="{{ old('district') ?: $client->district }}" name="district" class="form-control" id="district" placeholder="District">
                    @if ($errors->has('district'))
                    <span class="help-block">
                        <strong>{{ $errors->first('district') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('state') ? ' has-error' : '' }}">
                <label for="state" class="col-sm-3 control-label">State</label>
                <div class="col-sm-9">
                    <input type="text" value="{{ old('state') ?: $client->state }}" name="state" class="form-control" id="state" placeholder="State">
                    @if ($errors->has('state'))
                    <span class="help-block">
                        <strong>{{ $errors->first('state') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('pincode') ? ' has-error' : '' }}">
                <label for="pincode" class="col-sm-3 control-label">Pincode</label>
                <div class="col-sm-9">
                    <input type="text" value="{{ old('pincode') ?: $client->pincode }}" name="pincode" class="number form-control" id="pincode" placeholder="Pincode Number">
                    @if ($errors->has('pincode'))
                    <span class="help-block">
                        <strong>{{ $errors->first('pincode') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('pan_number') ? ' has-error' : '' }}">
                <label for="pan_number" class="col-sm-3 control-label required">PAN Card Number</label>
                <div class="col-sm-9">
                    <input type="text" value="{{ old('pan_number') ?: $client->pan_number }}" name="pan_number" class="form-control" id="pan_number" placeholder="PAN Card Number">
                    @if ($errors->has('pan_number'))
                    <span class="help-block">
                        <strong>{{ $errors->first('pan_number') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('gst_number') ? ' has-error' : '' }}">
                <label for="gst_number" class="col-sm-3 control-label required">GST Number</label>
                <div class="col-sm-9">
                    <input type="text" value="{{ old('gst_number') ?: $client->gst_number }}" name="gst_number" class="form-control" id="gst_number" placeholder="GST Number">
                    @if ($errors->has('gst_number'))
                    <span class="help-block">
                        <strong>{{ $errors->first('gst_number') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('domain') ? ' has-error' : '' }}">
                <label for="domain" class="col-sm-3 control-label">Website</label>
                <div class="col-sm-9">
                    <input type="text" value="{{ old('domain') ?: $client->domain }}" name="domain" class="form-control" id="domain" placeholder="www.example.com">
                    @if ($errors->has('domain'))
                    <span class="help-block">
                        <strong>{{ $errors->first('domain') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group col-sm-10 col-sm-offset-1{{ ($errors->has('message_number') || $errors->has('message_number_cc')) ? ' has-error' : '' }}">
                <label for="message_number" class="col-sm-3 control-label">WhatsApp Mobile Number</label>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-6">
                            <select name="message_number_cc" class="select2">
                                <option value="">Country</option>
                                @foreach ($countries as $country)
                                <option {{ (old('message_number_cc') == $country->code || $client->message_number_cc == $country->code) ? 'selected' : '' }} value="{{ $country->code }}">{{ $country->name }}({{ $country->code }})</option>
                                @endforeach
                            </select>
                            @if ($errors->has('message_number_cc'))
                            <span class="help-block">
                                <strong>{{ $errors->first('message_number_cc') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-sm-6">
                            <input type="text" value="{{ old('message_number') ?: $client->message_number }}" name="message_number" class="form-control" id="message_number" placeholder="WhatsApp Mobile Number">
                            @if ($errors->has('message_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('message_number') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('messaging_key') ? ' has-error' : '' }}">
                <label for="messaging_key" class="col-sm-3 control-label">WhatsApp Account Key</label>
                <div class="col-sm-9">
                    <input type="text" value="{{ old('messaging_key') ?: $client->messaging_key }}" name="messaging_key" class="form-control" id="messaging_key" placeholder="WhatsApp Account Key">
                    @if ($errors->has('messaging_key'))
                    <span class="help-block">
                        <strong>{{ $errors->first('messaging_key') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('active') ? ' has-error' : '' }}">
                <label for="active" class="col-sm-3 control-label required">Status</label>
                <div class="col-sm-9">
                    <div class="radio pull-left">
                        <label>
                            <input type="radio" name="active" id="active" value="1" {{ (old('active') == '1' || '1' ==  $client->active) ? 'checked' : (empty($request->active) ? 'checked' : '') }}> Active
                        </label>
                    </div>
                    <div class="radio pull-left col-md-6">
                        <label>
                            <input type="radio" name="active" id="" value="0" {{ (old('active') == '0' || '0' ==  $client->active) ? 'checked' : '' }}> Inactive
                        </label>
                    </div>
                    @if ($errors->has('active'))
                    <span class="help-block">
                        <strong>{{ $errors->first('active') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="col-sm-10 col-sm-offset-1">
                <button type="submit" class="btn btn-success btn-flat">Save Changes</button>&nbsp;&nbsp;
                <a href="{{ route('admin.client.home') }}" class="btn btn-default btn-flat">Cancel</a>
            </div>
        </div>
        <!-- /.box-footer -->
    </form>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}" />
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" />
@endsection
@section('scripts')
<script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script>
    $(function() {
        $('.select2').select2();
        $('textarea.editor').wysihtml5();
    });
</script>
@endsection
