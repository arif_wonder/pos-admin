@extends('layouts.admin')
@section('title', 'Store Admins')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Store Admins</h3>
        <div class="pull-right">
            <a href="{{ route('admin.client.add') }}" class="btn btn-success btn-flat btn-sm"><i class="fa fa-plus"></i> Add New</a>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="data-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>S.N.</th>
                    <th>Username</th>
                    <th>E-mail ID</th>
                    <th>Name</th>
                    <th>Mobile Number</th>
                    <th>Stores Allowed</th>
                    <th>Business Type</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @if (count($clients))
                @foreach ($clients as $client)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $client->username }}</td>
                    <td>{{ $client->email }}</td>
                    <td>{{ $client->name }}</td>
                    <td>{{ $client->mobile }}</td>
                    <td>{{ $client->max_stores }}</td>
                    <td>{{ $client->business_type_id ? $client->businessType->name : '' }}</td>
                    <td>
                        @if ($client->active == '1')
                        <a href="{{ route('admin.client.status', ['id'=>$client->id, 'status' => '0']) }}" data-toggle="tooltip" title="Click to deactivate" class="btn btn-success btn-flat btn-xs"><i class="fa fa-unlock"></i> Active</a>
                        @else
                        <a href="{{ route('admin.client.status', ['id'=>$client->id, 'status' => '1']) }}" data-toggle="tooltip" title="Click to activate" class="btn btn-warning btn-flat btn-xs"><i class="fa fa-lock"></i> Inactive</a>
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('admin.client.edit', ['id'=>$client->id]) }}" data-toggle="tooltip" title="Click to edit" class="btn btn-info btn-flat btn-sm"><i class="fa fa-pencil"></i> </a>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('plugins/datatables/css/dataTables.bootstrap.min.css') }}" />
@endsection
@section('scripts')
<script src="{{ asset('plugins/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(function() {
        $('#data-table').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            "aoColumnDefs": [{
                'bSortable': false,
                'aTargets': [6]
            }]
        });
    });
</script>
@endsection
