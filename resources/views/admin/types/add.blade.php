@extends('layouts.admin')
@section('title', 'Add Business Type')

@section('content')
<div class="box box-success box-12">
    <div class="box-header with-border">
        <h3 class="box-title">Add Business Type</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" method="post" action="{{ route('admin.business.add') }}">
        {{ csrf_field() }}
        <div class="box-body">
            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-sm-3 control-label required">Name</label>
                <div class="col-sm-9">
                    <input type="text" value="{{ old('name') }}" name="name" class="form-control" id="name" placeholder="Business Types">
                    @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group col-sm-10 col-sm-offset-1{{ $errors->has('active') ? ' has-error' : '' }}">
                <label for="active" class="col-sm-3 control-label required">Status</label>
                <div class="col-sm-9">
                    <div class="radio pull-left">
                        <label>
                            <input type="radio" name="active" id="active" value="1" {{ old('active') == '1' ? 'checked' : (empty($request->active) ? 'checked' : '') }}> Active
                        </label>
                    </div>
                    <div class="radio pull-left col-md-6">
                        <label>
                            <input type="radio" name="active" id="" value="0" {{ old('active') == '0' ? 'checked' : '' }}> Inactive
                        </label>
                    </div>
                    @if ($errors->has('active'))
                    <span class="help-block">
                        <strong>{{ $errors->first('active') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="col-sm-10 col-sm-offset-1">
                <button type="submit" class="btn btn-success btn-flat">Save Changes</button>&nbsp;&nbsp;
                <a href="{{ route('admin.business.home') }}" class="btn btn-default btn-flat">Cancel</a>
            </div>
        </div>
        <!-- /.box-footer -->
    </form>
</div>
@endsection
