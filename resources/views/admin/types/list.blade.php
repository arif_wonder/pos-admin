@extends('layouts.admin')
@section('title', 'Business Types')

@section('content')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Business Types</h3>
        <div class="pull-right">
            <a href="{{ route('admin.business.add') }}" class="btn btn-success btn-flat btn-sm"><i class="fa fa-plus"></i> Add New</a>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="data-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>S.N.</th>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @if (count($types))
                @foreach ($types as $type)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $type->name }}</td>
                    <td>{{ $type->slug }}</td>
                    <td>
                        @if ($type->active == '1')
                        <a href="{{ route('admin.business.status', ['id'=>$type->id, 'status' => '0']) }}" data-toggle="tooltip" title="Click to deactivate" class="btn btn-success btn-flat btn-xs"><i class="fa fa-unlock"></i> Active</a>
                        @else
                        <a href="{{ route('admin.business.status', ['id'=>$type->id, 'status' => '1']) }}" data-toggle="tooltip" title="Click to activate" class="btn btn-warning btn-flat btn-xs"><i class="fa fa-lock"></i> Inactive</a>
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('admin.business.edit', ['id'=>$type->id]) }}" data-toggle="tooltip" title="Click to edit" class="btn btn-info btn-flat btn-sm"><i class="fa fa-pencil"></i> </a>
                        <form method="post" action="{{ route('admin.business.delete') }}" class="delete-form" id="form-{{$type->id}}">
                            <input type="hidden" name="_method" value="delete" />
                            <input type="hidden" name="id" value="{{$type->id}}" />
                            {!! csrf_field() !!}
                            <button data-toggle="tooltip" type="submit" title="Click to delete" class="btn btn-danger btn-flat btn-sm"><i class="fa fa-trash"></i> </button>
                        </form>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
@endsection
@section('styles')
<link rel="stylesheet" href="{{ asset('plugins/datatables/css/dataTables.bootstrap.min.css') }}" />
@endsection
@section('scripts')
<script src="{{ asset('plugins/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(function() {
        $('#data-table').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            "aoColumnDefs": [{
                'bSortable': false,
                'aTargets': [4]
            }]
        });
    });
</script>
@endsection
