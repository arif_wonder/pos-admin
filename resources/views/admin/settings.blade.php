@extends('layouts.admin')
@section('title', 'Application Settings')
@section('content')

@php
// dd($data)
@endphp
<div class="box box-warning box-8">
    <div class="box-header with-border">
      <h3 class="box-title">Application Settings</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" method="post" action="{{ route('admin.setting') }}">
        {{ csrf_field() }}
      <div class="box-body">
        <div class="form-group{{ $errors->has('cand_cancellation_charge') ? ' has-error' : '' }}">
          <label for="cand_cancellation_charge" class="col-sm-4 control-label required">Candidate Cancellation Charge</label>
          <div class="col-sm-6">
            <input type="number" class="form-control" name="setting[cand_cancellation_charge]" id="cand_cancellation_charge" value="{{ old('cand_cancellation_charge') ?: ($data && $data['cand_cancellation_charge']) ? $data['cand_cancellation_charge'] : '' }}" step="any" placeholder="example: 10.00">
            @if ($errors->has('cand_cancellation_charge'))
            <span class="help-block">
                <strong>{{ $errors->first('cand_cancellation_charge') }}</strong>
            </span>
            @endif
          </div>
          <div class="col-sm-2 no-padding no-margin">
              <strong>%</strong>
          </div>
        </div>
        <div class="form-group{{ $errors->has('emp_cancellation_charge') ? ' has-error' : '' }}">
          <label for="emp_cancellation_charge" class="col-sm-4 control-label required">Employer Cancellation Charge</label>
          <div class="col-sm-6">
            <input type="number" class="form-control" name="setting[emp_cancellation_charge]" id="emp_cancellation_charge" value="{{ old('emp_cancellation_charge') ?: ($data && $data['emp_cancellation_charge']) ? $data['emp_cancellation_charge'] : '' }}" step="any" placeholder="example: 10.00">
            @if ($errors->has('emp_cancellation_charge'))
            <span class="help-block">
                <strong>{{ $errors->first('emp_cancellation_charge') }}</strong>
            </span>
            @endif
          </div>
          <div class="col-sm-2 no-padding no-margin">
              <strong>%</strong>
          </div>
        </div>
        <div class="form-group{{ $errors->has('cand_compensation_charge') ? ' has-error' : '' }}">
          <label for="cand_compensation_charge" class="col-sm-4 control-label required">Candidate Compensation Charge</label>
          <div class="col-sm-6">
            <input type="number" class="form-control" name="setting[cand_compensation_charge]" id="cand_compensation_charge" value="{{ old('cand_compensation_charge') ?: ($data && $data['cand_compensation_charge']) ? $data['cand_compensation_charge'] : '' }}" step="any" placeholder="example: 10.00">
            @if ($errors->has('cand_compensation_charge'))
            <span class="help-block">
                <strong>{{ $errors->first('cand_compensation_charge') }}</strong>
            </span>
            @endif
          </div>
          <div class="col-sm-2 no-padding no-margin">
              <strong>%</strong>
          </div>
        </div>
        <div class="form-group{{ $errors->has('emp_payin_charge') ? ' has-error' : '' }}">
          <label for="emp_payin_charge" class="col-sm-4 control-label required">Employer PayIn Charge</label>
          <div class="col-sm-6">
            <input type="number" class="form-control" value="{{ old('emp_payin_charge') ?: ($data && $data['emp_payin_charge']) ? $data['emp_payin_charge'] : '' }}" step="any" name="setting[emp_payin_charge]" id="emp_payin_charge" placeholder="example: 10.00">
            @if ($errors->has('emp_payin_charge'))
            <span class="help-block">
                <strong>{{ $errors->first('emp_payin_charge') }}</strong>
            </span>
            @endif
          </div>
          <div class="col-sm-2 no-padding no-margin">
              <strong>%</strong>
          </div>
        </div>
        <div class="form-group{{ $errors->has('emp_payout_charge') ? ' has-error' : '' }}">
          <label for="emp_payout_charge" class="col-sm-4 control-label required">Employer PayOut Charge</label>
          <div class="col-sm-6">
            <input type="number" class="form-control" name="setting[emp_payout_charge]" id="emp_payout_charge" step="any" value="{{ old('emp_payout_charge') ?: ($data && $data['emp_payout_charge']) ? $data['emp_payout_charge'] : '' }}" placeholder="example: 10.00">
            @if ($errors->has('emp_payout_charge'))
            <span class="help-block">
                <strong>{{ $errors->first('emp_payout_charge') }}</strong>
            </span>
            @endif
          </div>
          <div class="col-sm-2 no-padding no-margin">
              <strong>%</strong>
          </div>
        </div>
        <div class="form-group{{ $errors->has('cand_payout_charge') ? ' has-error' : '' }}">
          <label for="cand_payout_charge" class="col-sm-4 control-label required">Candidate Pay Out Charge</label>
          <div class="col-sm-6">
            <input type="number" class="form-control" name="setting[cand_payout_charge]" id="cand_payout_charge" step="any" value="{{ old('cand_payout_charge') ?: ($data && $data['cand_payout_charge']) ? $data['cand_payout_charge'] : '' }}" placeholder="example: 10.00">
            @if ($errors->has('cand_payout_charge'))
            <span class="help-block">
                <strong>{{ $errors->first('cand_payout_charge') }}</strong>
            </span>
            @endif
          </div>
          <div class="col-sm-2 no-padding no-margin">
              <strong>%</strong>
          </div>
        </div>

      </div>
      <!-- /.box-body -->
      <div class="box-footer">
          <button type="submit" class="btn btn-warning btn-flat">Save Changes</button>&nbsp;&nbsp;&nbsp;
          <a href="{{ route('admin.home') }}" class="btn btn-default btn-flat">Cancel</a>
      </div>
      <!-- /.box-footer -->
    </form>
  </div>
@endsection
