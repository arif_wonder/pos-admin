<ol class="breadcrumb">
	<li><a href="{{ route('admin.home') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	@foreach ($breadcrumbs as $link)
		@if ($loop->last)
		<li class="active">
			{{ $link['label'] }}
		</li>
		@else
		<li>
			@if ($link['link'])
				<a href="{{ $link['link'] }}">
			@endif
			{{ $link['label'] }}
			@if ($link['link'])
				</a>
			@endif
		</li>
		@endif
	@endforeach
</ol>
<div class="clearfix"></div>
