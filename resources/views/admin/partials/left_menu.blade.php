<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('img/avatar.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Navigation</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-group"></i> <span>Store Admins</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.client.home') }}"><i class="fa fa-group"></i> All Store Admins</a></li>
                    <li><a href="{{ route('admin.client.add') }}"><i class="fa fa-plus"></i> Add New</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-briefcase"></i> <span>Business Types</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.business.home') }}"><i class="fa fa-briefcase"></i> All Business Types</a></li>
                    <li><a href="{{ route('admin.business.add') }}"><i class="fa fa-plus"></i> Add New</a></li>
                </ul>
            </li>
            <li><a href="{{ route('admin.password.form') }}"><i class="fa fa-key"></i> <span>Change Password</span></a></li>
            <!-- <li><a href="{{ route('admin.setting') }}"><i class="fa fa-cogs"></i> Settings</a></li> -->
            <li><a href="{{ route('logout') }}"><i class="fa fa-power-off"></i> Sign Out</a></li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
