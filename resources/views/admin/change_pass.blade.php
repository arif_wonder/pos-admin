@extends('layouts.admin')
@section('title', 'Change Password')

@section('content')
<div class="box box-warning box-7">
    <div class="box-header with-border">
      <h3 class="box-title">Change Password</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" method="post" action="{{ route('admin.password.change') }}">
        {{ csrf_field() }}
      <div class="box-body">
        <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
          <label for="old_password" class="col-sm-2 control-label required">Old Password</label>
          <div class="col-sm-10">
            <input type="password" class="form-control" name="old_password" id="old_password" placeholder="Old Password">
            @if ($errors->has('old_password'))
            <span class="help-block">
                <strong>{{ $errors->first('old_password') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <label for="password" class="col-sm-2 control-label required">New Password</label>
          <div class="col-sm-10">
            <input type="password" name="password" class="form-control" id="password" placeholder="New Password">
            @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
          <label for="inputPassword3" class="col-sm-2 control-label required">Confirm Password</label>
          <div class="col-sm-10">
            <input type="password" name="password_confirmation" class="form-control" id="inputPassword3" placeholder="Confirm Password">
            @if ($errors->has('password_confirmation'))
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
            @endif
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
          <button type="submit" class="btn btn-warning btn-flat">Save Changes</button>&nbsp;&nbsp;&nbsp;
          <a href="{{ route('admin.home') }}" class="btn btn-default btn-flat">Cancel</a>
      </div>
      <!-- /.box-footer -->
    </form>
  </div>
@endsection
