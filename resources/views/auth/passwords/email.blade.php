@extends('layouts.standalone')

@section('content')
<div class="register-box">
    <div class="register-logo">
        <a href="{{ url('/') }}">{{ config('app.name', 'Mobiocean POS') }}</a>
    </div>

    <div class="register-box-body">
        <p class="login-box-msg">Reset Password</p>
        @if (session('status'))
            <div class="alert alert-success alert-dismissable">{{ session('status') }}
                <button class="close" data-dismiss="alert">&times;</button>
            </div>
        @endif
        <form action="{{ route('password.email') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="text" name="email" class="form-control" placeholder="E-mail ID" value="{{ old('email') }}" required>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-flat">Send Password Reset Link</button>
            </div>
        </form>
    </div>
</div>

@endsection
