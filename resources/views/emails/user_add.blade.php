@component('mail::message')
Hello <strong>{{ $client->name }}</strong>!

Welcome to {{ config('app.name') }}. Your account has been created successfully.<br/><br/>
Your login credentials are -<br/>
Email Address : <span style="font-weight:bold;">{{ $client->email }}</span><br/>
Password      : <span style="font-weight:bold;">{{ env('DEFAULT_USER_PASS') }}</span><br/>

With Regards,<br>
{{ config('app.name') }} Team
@endcomponent
