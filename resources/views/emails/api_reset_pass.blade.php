@component('mail::message')
Hello {{ $name }}!

You are receiving this email because we received a password reset request for your account.<br/>Below is the OTP to reset your password.

@component('mail::promotion')
<strong style="font-size: 35px;">{{ $otp->otp }}</strong>
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
