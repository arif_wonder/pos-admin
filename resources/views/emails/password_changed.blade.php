@component('mail::message')
Hello {{ $name }}!

Your password has been changed recently from our admin portal.<br/>If you have not requested for the same please report to our support.

@component('mail::button', ['url' => route('home')])
Report
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
