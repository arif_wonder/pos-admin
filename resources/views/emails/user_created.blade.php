@component('mail::message')
Hello <strong>{{ $user->username }}</strong>!

Welcome to {{ config('app.name') }}. Your account has been created successfully.<br /><br />
Your login credentials are -<br />
Email Address : <span style="font-weight:bold;">{{ $user->email }}</span><br />
Password : <span style="font-weight:bold;">{{ $pass }}</span><br />

With Regards,<br>
{{ config('app.name') }} Team
@endcomponent
