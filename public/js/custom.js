/*
* Select all text of input field
*/
$(document).on('click', '.select_all', function () {
  $(this).select();
});
/*
* Allow only numeric value in input field ex: 20, 30
*/
$(document).on('keypress keyup blur', '.number', function (event) {
  $(this).val($(this).val().replace(/[^\d].+/, ""));
  if ((event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }
});
/*
* Allow only decimal value in input field ex: 20.59, 30.00
* This function not alow more than one decimal point
*/
$(document).on('keypress keyup blur', '.decimal', function (event) {
  $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
    event.preventDefault();
  }
});
//
$(document).on('keypress keyup blur', '.string', function (event) {
  $(this).val($(this).val().replace(/[*|,\\":<>\[\]{}`';()@&$#%!+-]/g, ''));
  var k = event.keyCode;
  $return = ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57)); ////k == 32  ||
  if(!$return) {
    return false;
  }
});

$(function(){
	$(document).on("cut copy paste",".notccp",function(e) {
        e.preventDefault();
    });
    $( document ).on( 'focus', ':input', function(){
        $( this ).attr( 'autocomplete', 'off' );
    });
    $('[type="submit"]').not('.delete-form > [type="submit"]').click(function(e) {
        $(this).attr('disable', true);
        $('#loader').show();
    });

    // confirm box.
    $('.delete-form [type="submit"]').click(function () {
        var _this = $(this);
        swal("Are you sure?", "Once deleted, you will not be able to recover!", {
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                _this.closest('form').submit();
            }
        });
        return false;
    });
})
